var case_data = null;

func['cases'] = function(){
	//show_cv();	

	check_user_type(function(){
		$('#admin-panel').css({'display':'unset'});
		$('#admin-btn').css({'display':'unset'});
	    $('#admin-btn').click(function(e){
			show_admin();
		});
		$('#normal-btn').click(function(e){
			show_normal();
		});

	}, function(){
		$('#admin-panel').css({'display':'none'});
		$('#admin-btn').css({'display':'none'});
		normal_view();	
	
	});
    display('cir-4');
	document.getElementById('cases-page').innerHTML += get_pln_add();
	
	$('#pln_submit_btn').click(function(e){

	});
    $('.circle').click(function(e){
    	console.log(this.id);
    	display(this.id);

    });

    $('#cv-x').click(function(e){
    	hide_cv();
    });

/*    $('.case_btn_view').click(function(e){
    	console.log(this.class);
    	//view(this.class);
    });*/

};
/*------------------------------- NORMAL VIEW --------------------------------------*/

function normal_view(){
    online(function(){
		$.ajax({
	        type:'POST',
	        url:'http://www.roh.nurmaan.free-staging-server.us/api/normal_case',
	        dataType:'json',
	        data:{user_id:user_id},
	        success:function(data){
                if(data.result){
                	console.log(data.data);
                	display_normal_view(data.data);

                }else{
                	notif('Failed to load data. Please try again');
                }

	        },
	        error:function(data){
	            notif('Failed to load data. Please try again');
	        }
	    });
	}, function(){
		notif('Sorry, could not process your request because you are offline');
	});	
}

function display_normal_view(data){
	var targ = document.getElementById('normal-panel');
	document.getElementById('normal-body').innerHTML ='';
	var cont = '';

    for (var i = 0; i < data.length; i++) {
    	console.log(data[i]);
    	cont += 
    	"<div class = 'row center'>"+
    	    "<div class = 'card cd-full'>"+

	    	    "<div class = 'row'>"+
	    	        "<div class = 'cd-title'>"+data[i]['title']+"</div>"+
	    	    "</div>"+

	    	    "<div class='pln-box'>"+
		    			"<div class='pln-box-title'>Description</div>"+
		    			"<div class='row center' style = 'word-wrap:break-word;'>"+data[i]['overview']+"</div>"+
		    	"</div>"+

	    	    "<div class='pln-box'>"+
		    			"<div class='pln-box-title'>Members of Family</div>"+
		    			"<div class='row center'>"+data[i]['member']+"</div>"+
		    	"</div>"+

		    	"<div class='pln-box'>"+
		    			"<div class='pln-box-title'>Issues</div>"+
		    			"<div id = 'cases-normal-issues-"+i+"'></div>"+
		    	"</div>"+

		    	"<div class='pln-box'>"+
		    			"<div class='pln-box-title'>Solutions</div>"+
		    			"<div id = 'cases-normal-solutions-"+i+"'></div>"+
		    	"</div>"+

		    	"<div class='pln-box'>"+
		    			"<div class='pln-box-title'>Condition</div>"+
		    			"<div class='row center'>"+data[i]['cond']+"/100</div>"+
		    	"</div>"+

		    	"<div class='pln-box'>"+
		    			"<div class='pln-box-title'>Message Us</div>"+
		    			"<div class='row center'>Please provide all the neccessary details about the help you want to provide. Feel free to provide solutions other than the ones we have mentioned.</div>"+
		    			"<div class ='row center'><textarea class = 'add-input auto-height' placeholder='Specify the help you want to provide' id='help'></textarea></div>"+
		    	"</div>"+
                
		    	"<div class = 'add-btn' onclick='help(\""+data[i]['u_id']+"\")'>Help</div>"+

    	    "</div>"+
    	"</div>";

    	document.getElementById('normal-body').innerHTML += cont;
	    var issue = [];
	    if(data[i]['issue'] != null){
	    	issue =JSON.parse(data[i]['issue']);
	    }
	    var issue_cont ="";

	    var solution = [];
	    if(data[i]['solution'] != null){
	    	solution =JSON.parse(data[i]['solution']);
	    }
	    var solution_cont ='';

	    for(var j = 0; j<Object.keys(issue).length; j++){
	        issue_cont += "<div class='row'> - "+issue[i]+"</div>";
	    }

	    for (var j = 0; j < Object.keys(solution).length; j++){
	    	if(solution[j]['state'] != 'unchecked' && solution[j]['state'] != 'added-unchecked' ){
	    		solution_cont += "<div class='row'> - "+solution[j]['solution']+"</div>";
	    	}
	    	
	    }

	    document.getElementById("cases-normal-issues-"+i).innerHTML = issue_cont;
	    document.getElementById("cases-normal-solutions-"+i).innerHTML = solution_cont;
	}

   
}
/*------------------------------------------- ADMIN -----------------------------------*/

function display (thisid){
	disp_over('Received data for this stage. Displaying it...');
	var id = thisid.slice(-1);console.log(id);
	//ca[id]();
	online(function(){
		$.ajax({
	        type:'POST',
	        url:'http://www.roh.nurmaan.free-staging-server.us/api/cases',
	        dataType:'json',
	        data:{stage:id, u_id:user_id},
	        success:function(data){
                if(data.result){                	
                	case_data = data.data;
                	console.log(data.data);
                	ca[id](data.data);

                }else{
                	notif('Failed to load data. Please try again');
                }

	        },
	        error:function(data){
	            notif('Failed to load data. Please try again');
	        }
	    });
	}, function(){
		notif('Sorry, could not process your request because you are offline');
	});
}

/*--------------------------------------cv---------------------------------------------*/
function case_view (u_id, stage){
	console.log(u_id);
    online(function(){
		$.ajax({
	        type:'POST',
	        url:'http://www.roh.nurmaan.free-staging-server.us/api/cv',
	        dataType:'json',
	        data:{stage:stage, id:u_id, u_id:user_id},
	        success:function(data){
                if(data.result){
                	console.log(data.data);
                	display_cv(data.data);

                }else{
                	notif('Failed to load data. Please try again');
                }

	        },
	        error:function(data){
	            notif('Failed to load data. Please try again');
	        }
	    });
	}, function(){
		notif('Sorry, could not process your request because you are offline');
	});	
}

function display_cv(data){
	var targ = document.getElementById('cv');
	cont = 
	            "<div class='row center'>"+
	                "<div id='cv-head'>"+
	                    "<div id='cv-x'>X</div>"+
	                "</div>"+
	            "</div>"+
	            "<div id='cv-cards'>"+
					"<div class='row center cv-card'>"+
						"<div class='card cd-full cd-cv'>"+
							"<div class='cd-title'>General Information</div>"+

							"<div class='cv-container'>"+
								"<div class='cv-title'>Additional Notes</div>"+
								"<div class='cv-adds'>"+data[0]['client']['adds']+"</div>"+
							"</div>"+
							"<div class='cv-container' id='cv-issue-container'>"+
								"<div class='cv-title'>Issues</div>"+
							"</div>"+
							"<div class='cv-container' id='cv-solution-container'>"+
								"<div class='cv-title'>Solutions</div>"+
							"</div>"+
							"<div class='cv-container'>"+
								"<div class='cv-title'>Region</div>"+
								"<div class='cv-adds'>"+data[0]['client']['region']+"</div>"+
							"</div>"+
							"<div class='cv-container'>"+
								"<div class='cv-title'>Address</div>"+
								"<div class='cv-adds'>"+data[0]['client']['address']+"</div>"+
							"</div>"+
							"<div class='cv-container'>"+
								"<div class='cv-title'>Coordinates</div>"+
								"<div class='cv-adds'>"+data[0]['client']['coordinates']+"</div>"+
							"</div>"+
							"<div class='cv-container'>"+
								"<div class='cv-title'>Home Number</div>"+
								"<div class='cv-adds'>"+data[0]['client']['home']+"</div>"+
							"</div>"+
							"<div class='cv-container'>"+
								"<div class='cv-title'>Members of Family</div>"+
								"<div class='cv-adds'>"+data[0]['member'].length+"</div>"+
							"</div>"+
							"<div class='cv-container'>"+
								"<div class='cv-title'>Condition</div>"+
								"<div class='cv-adds'>"+data[0]['client']['cond']+"/100</div>"+
							"</div>"+
							"<div class='cv-container'>"+
								"<div class='cv-title'>case ID</div>"+
								"<div class='cv-adds'>"+data[0]['client']['u_id']+"/100</div>"+
							"</div>"+
						"</div>"+
					"</div>"+
				"</div>";

	    cont = cont.replace(/undefined/g, 'Case Validation Required First');
	    targ.innerHTML = cont;

	display_cv_member(data[0]['member']);
	if(data[0]['client']['issue'] != "" && data[0]['client']['solution'] != "" ){
		display_cv_situation({'issue':JSON.parse(data[0]['client']['issue'], true), 'solution':JSON.parse(data[0]['client']['solution'], true)});

	}
	display_cv_validated(data[0]['validate_1'],'First');
	if(typeof data[0]['validate_3'] != 'undefined'){		
	    display_cv_validated(data[0]['validate_3'],'Second');
	}
	display_cv_remove(data[0]['remove']);
	display_cv_photo(data);
	show_cv();
}

function display_cv_situation(data){
	console.log(data);
	var issue = data['issue'];
	var solution = data['solution'];
    var targ = document.getElementById('cv-issue-container');

	for(var i = 0; i < issue.length; i++){
		targ.innerHTML += "<div class='cv-issue'>- "+issue[i]+"</div>";
	}

	var tar = document.getElementById('cv-solution-container');

	for(var i = 0; i < solution.length; i++){
		tar.innerHTML += "<div class='cv-solution cv-"+solution[i]['state']+"'>- "+solution[i]['solution']+"</div>";
	}
}

function display_cv_member(mem){
	//console.log(mem);
	var targ = document.getElementById('cv-cards');
	cont ='';
    for(var i=0; i < mem.length;i++){
        cont += "<div class='row center cv-card'>"+
						"<div class='card cd-full cd-cv'>"+
							"<div class='cd-title'>Member "+(i+1)+"</div>"+

							"<div class='cv-container'>"+
								"<div class='cv-title'>First Name</div>"+
								"<div class='cv-adds'>"+mem[i]['first_name']+"</div>"+
							"</div>"+

							"<div class='cv-container'>"+
								"<div class='cv-title'>Last Name</div>"+
								"<div class='cv-adds'>"+mem[i]['last_name']+"</div>"+
							"</div>"+
							"<div class='cv-container'>"+
								"<div class='cv-title'>Other Names</div>"+
								"<div class='cv-adds'>"+mem[i]['other_name']+"</div>"+
							"</div>"+
							"<div class='cv-container'>"+
								"<div class='cv-title'>NIC Number</div>"+
								"<div class='cv-adds'>"+mem[i]['nic']+"</div>"+
							"</div>"+
							"<div class='cv-container'>"+
								"<div class='cv-title'>Date of Birth</div>"+
								"<div class='cv-adds'>"+mem[i]['dob']+"</div>"+
							"</div>"+
							"<div class='cv-container'>"+
								"<div class='cv-title'>Age</div>"+
								"<div class='cv-adds'>"+mem[i]['age']+"</div>"+
							"</div>"+
							"<div class='cv-container'>"+
								"<div class='cv-title'>Mobile Number</div>"+
								"<div class='cv-adds'>"+mem[i]['mobile']+"</div>"+
							"</div>"+
							"<div class='cv-container'>"+
								"<div class='cv-title'>Email Address</div>"+
								"<div class='cv-adds'>"+mem[i]['email']+"</div>"+
							"</div>"+
							"<div class='cv-container'>"+
								"<div class='cv-title'>Occupation</div>"+
								"<div class='cv-adds'>"+mem[i]['occupation']+"</div>"+
							"</div>"+
							"<div class='cv-container'>"+
								"<div class='cv-title'>Monthly Salary</div>"+
								"<div class='cv-adds'>"+mem[i]['salary']+"</div>"+
							"</div>"+
						"</div>"+
					"</div>";
    }
    cont = cont.replace(/undefined/g,'Case Validation Required First');
    targ.innerHTML+=cont;
}

function display_cv_validated(val, stage){
	console.log(val);
    document.getElementById('cv-cards').innerHTML += 
                    "<div class='row center cv-card'>"+
						"<div class='card cd-full cd-cv'>"+
							"<div class='cd-title'>"+stage+" Validation</div>"+
							"<div class='cv-container' id='cv-validated-"+stage+"'>"+
							"</div>"+
						"</div>"+
					"</div>";
    var targ = document.getElementById('cv-validated-'+stage);
    var cont = '';
    for(var i = 0; i <val.length; i++){
        cont += '<div>- '+val[i]['first_name']+' '+val[i]['last_name']+'</div>';
    }
    targ.innerHTML += cont;
}

function display_cv_remove(rem){
	console.log(rem);
    document.getElementById('cv-cards').innerHTML += 
                    "<div class='row center cv-card'>"+
						"<div class='card cd-full cd-cv'>"+
							"<div class='cd-title'>Removed By</div>"+
							"<div class='cv-container' id='cv-removed'>"+
							"</div>"+
						"</div>"+
					"</div>";
    var targ = document.getElementById('cv-removed');
    var cont = '';
    for(var i = 0; i <rem.length; i++){
        cont += '<div>- '+rem[i]['first_name']+' '+rem[i]['last_name']+'</div>';
    }
    console.log(cont);
    targ.innerHTML += cont;
}


function display_cv_photo(data){
	online(function(){
		$.ajax({
	        type:'POST',
	        url:'http://www.roh.nurmaan.free-staging-server.us/api/cv_photo',
	        dataType:'json',
	        data:{user_id:user_id, case_id:data[0]['client']['u_id']},
	        success:function(data){
                if(data.result){
                	console.log(data.data);
                    document.getElementById('cv-cards').innerHTML += 
	                    "<div class='row center cv-card' id='img-cont-cv'></div>";
			    	for(var i =0; i< data.data.length; i++){
			            if(data.data[i] != null){
			            	var el0 = document.createElement('div'); setAttributes(el0, {'class':'img-cont', 'id':'img-'+i});
						    var el1= document.createElement('img'); setAttributes(el1, {'src':'http://www.roh.nurmaan.free-staging-server.us/Users/Pictures/'+data.data[i], 'class':'add-img'});
						    var el2= document.createElement('div'); setAttributes(el2, {'class':'row center'});
						    el2.appendChild(el1);
						    el0.appendChild(el2);

						    var targ = document.getElementById('img-cont-cv');
						    targ.appendChild(el0);
						    init_pln_photo();
			            }else{
			                	notif('Failed to load data. Please try again');
			                }
			        }
			    }

	        },
	        error:function(data){
	            notif('Failed to load data. Please try again');
	        }
	    });
	}, function(){
		notif('Sorry, could not process your request because you are offline');
	});	
}
function show_cv(){
	var time =1000;
    $('#cv').css({'opacity':'0', 'display':'unset', 'z-index':'1000'});
    $('#cv-cards').css({'top':'100vh'});
    $('#cv').animate({'opacity':'1'}, time);
    setTimeout(function(){
        $('#cv-cards').animate({'top':'-4em'}, time).animate({'top':'2em'}, time/3).animate({'top':'0px'}, time/4);
    }, time/2);

    $('#cv-x').click(function(e){
    	hide_cv();
    });
}

function hide_cv(){
    var time = 750;
    $('#cv-cards').animate({'top':'-4em'}, time/3).animate({'top':'100vh'},time);
    setTimeout(function(){
    	$('#cv').animate({'opacity':'0'}, time/2);
    }, time/1.33);
    setTimeout(function(){
    	$('#cv').css({'display':'hidden', 'z-index':'-10'});
    }, 1.75*time)

}
/*--------------------------------------ca-----------------------------------------------*/

ca[1] = function(data){	
	var targ = document.getElementById('cases-main');
	var cont = "";
	for(var i = 0; i<data.length; i++){
        cont += 
                "<div class = 'case-cont'>"+
					"<div class='case-name'>"+data[i]['client']['title']+"</div>"+
                    "<div class='row center'>"+
					    "<div class='add-btn case_btn case_btn_view sg1 u_id"+data[i]['client']['u_id']+"'>View</div>"+  
					    "<div class='add-btn case_btn case_btn_validate sg1 sg1 u_id"+data[i]['client']['u_id']+"'>Validate("+data[i]['validate_1'].length+")</div>"+  
					    "<div class='add-btn case_btn case_btn_remove u_id"+data[i]['client']['u_id']+"'>Remove("+data[i]['remove'].length+")</div>"+
                    "</div>"+
				"</div>";
	}	
	targ.innerHTML = "<div id ='cv'></div>"+"<div id='cir-card' class='card cd-cases'><div class = 'cd-title'>Preliminary Screening</div>"+ cont + '</div>';
	case_listener();
}

ca[2] = function(data){
	var targ = document.getElementById('cases-main');
	var cont = "";
	for(var i =0; i<data.length; i++){
        cont += "<div class = 'case-cont'>"+
                    "<div class='case-name'>"+data[i]['client']['title']+"</div>"+
                    "<div class='row center'>"+  
					    "<div class='add-btn case_btn case_btn_remove sg2 u_id"+data[i]['client']['u_id']+"'>Remove("+data[i]['remove'].length+")</div>"+
					    "<div class='add-btn case_btn case_btn_view sg2 u_id"+data[i]['client']['u_id']+"'>View</div>"+
					    "<div class='add-btn case_btn case_btn_schedule st1 sg2 id"+i+" u_id"+data[i]['client']['u_id']+"'>Schedule</div>"+
                    "</div>"+
				"</div>";
	}
	targ.innerHTML = "<div id ='cv'></div>"+"<div id='cir-card' class='card cd-cases'><div class = 'cd-title'>Schedule 1st Site Visit</div>"+ cont  + '</div>';
    case_listener();
}
ca[3]  = function(data){
	var targ = document.getElementById('cases-main');
	var cont = "";
	for(var i =0; i<data.length; i++){
        cont += "<div class = 'case-cont'>"+
					"<div class='case-name'>"+data[i]['client']['title']+"</div>"+
                    "<div class='row center'>"+
					    "<div class='add-btn case_btn case_btn_view sg3 u_id"+data[i]['client']['u_id']+"'>View</div>"  +  
					    "<div class='add-btn case_btn case_btn_validate sg3 u_id"+data[i]['client']['u_id']+"'>Validate("+data[i]['validate_3'].length+")</div>"  +  
					    "<div class='add-btn case_btn case_btn_remove sg3 u_id"+data[i]['client']['u_id']+"'>Remove("+data[i]['remove'].length+")</div>"+
                    "</div>"+
				"</div>";
	}
	targ.innerHTML = "<div id ='cv'></div>"+"<div id='cir-card' class='card cd-cases'><div class = 'cd-title'>Validate Case</div>"+cont+'</div>';
	case_listener();
}
ca[4] = function(data){
	console.log(data);
	var targ = document.getElementById('cases-main');
	var cont = "";

	for(var i =0; i<data.length; i++){
		var soln_cont = '';
		var solution = JSON.parse(data[i]['client']['solution']);
		console.log(solution);
		for(var j=0; j<Object.keys(solution).length;j++){
			var k  = i+1;
			var l = j+1;
			var pos = generate(3);
			soln_cont += 
			    "<div class = 'row center' style='font-size:0.6em;'>"+
			        "<input type='checkbox' id = 'ca-cb-"+pos+"' class = 'case-soln'/>"+
			        "<label for='ca-checkbox-"+pos+"' class= 'ca-soln-"+i+" cv-"+solution[j]['state']+"' id='lb-ca-cb-"+pos+"' style='width:100%;text-align:left;'>"+solution[j]['solution']+"</label>"+

			    "</div>";
		}
        cont += "<div class = 'case-cont'>"+
					"<div class='case-name'>"+data[i]['client']['title']+"</div>"+
					    "<div class='case-goods-cont'>"+
					        soln_cont+
			                "<div id = 'case-goods-"+i+"'></div>"+
			                "<div class='row center'><input type='text' placeholder='Solution' class='add-input case-goods-input id"+i+"' id = 'case-goods-input-"+i+"'></div>"+
			                "<div class='add-btn' id='add-issue-add-btn' onclick = 'case_send_goods("+i+")'>+Add Solution</div>"+
			            "</div>"+
                    "<div class='row center' style ='margin:0.5em 0 0.5em 0;'>"+
                        "<div class='add-btn case_btn case_btn_view sg4 u_id"+data[i]['client']['u_id']+"'>View</div>" + 
					    "<div class='add-btn case_btn case_btn_remove sg4 u_id"+data[i]['client']['u_id']+"'>Remove("+data[i]['remove'].length+")</div>"+
                    "</div>"+

                    "<div class='row center' style ='margin:0.5em 0 0.5em 0;'>"+
                        "<div class='add-btn case_btn case_btn_submitsoln sg4 id"+i+" u_id"+data[i]['client']['u_id']+"'>Submit</div>" + 
                        "<div class='add-btn case_btn case_btn_schedule st2 sg4 id"+i+" u_id"+data[i]['client']['u_id']+"'>Schedule</div>"+
                    "</div>"+

                    "<div class='row center' style ='margin:0.5em 0 0.5em 0;'>"+
                        "<div class='add-btn case_btn case_btn_pubhlp sg4 id"+i+" u_id"+data[i]['client']['u_id']+"'>Public Help</div>" +
                    "</div>"+
				"</div>";
	}
	targ.innerHTML = "<div id ='cv'></div>"+"<div id='cir-card' class='card cd-cases'><div class = 'cd-title'>Proposed Help</div>"+"<div id ='cv'></div>"+ cont +'</div>';
	case_listener();
	$('.case-soln').change(function(e){
		var cl = $('#lb-'+this.id).attr('class');
		var cla = cl.substr(0, cl.indexOf(' '));
		var cls = '';
		if($(this).is(':checked')){
		    cls = cla+' cv-checked';
		}else{
			cls = cla+' cv-unchecked';
		}
		setAttributes(document.getElementById('lb-'+this.id), {'class':cls});
	});
}
ca[5] = function(data){
	var targ = document.getElementById('cases-main');
	var cont = "";
	for(var i =0; i<data.length; i++){
        cont += "<div class = 'case-cont'>"+
					"<div class='case-name'>"+data[i]['client']['title']+"</div>"+
					"<input type='date' class='add-input' width='30' id = 'st-"+i+"'>"+
                    "<div class='row center'>"+  
					    "<div class='add-btn case_btn case_btn_remove sg5 u_id"+data[i]['client']['u_id']+"'>Remove("+data[i]['remove'].length+")</div>"+
					    "<div class='add-btn case_btn case_btn_view sg5 u_id"+data[i]['client']['u_id']+"'>View</div>"+
					    "<div class='add-btn case_btn case_btn_schedule st2 sg5 id"+i+" u_id"+data[i]['client']['u_id']+"'>Schedule</div>"+
                    "</div>"+
				"</div>";
	}
	targ.innerHTML = "<div id ='cv'></div>"+"<div id='cir-card' class='card cd-cases'><div class = 'cd-title'>Schedule 2nd Site Visit</div>"+ cont  + '</div>';
    case_listener();
}
ca[6] = function(data){	

	var targ = document.getElementById('cases-main');
	var cont = "";
	for(var i =0; i<data.length; i++){
		var soln_cont = '';
		var solution = JSON.parse(data[i]['client']['solution']);
		console.log(solution);
		for(var j=0; j<Object.keys(solution).length;j++){
			var k  = i+1;
			var l = j+1;
			var pos = power(((k+1)*(k+1) + l),(k+l));
			var chk = '';
			if(solution[j]['state'] == 'checked' || solution[j]['state'] == 'added'){
				console.log('yep');
			    	chk = 'checked';
			}
			soln_cont += 
			    "<div class = 'row center' style='font-size:0.6em;'>"+
			        "<input type='checkbox' id = 'ca-cb-"+pos+"' class = 'case-soln' "+chk+"/>"+
			        "<label for='ca-checkbox-"+pos+"' class= 'ca-soln-"+i+" cv-"+solution[j]['state']+"' id='lb-ca-cb-"+pos+"' style='width:100%;text-align:left;'>"+solution[j]['solution']+"</label>"+

			    "</div>";

		}
		console.log(soln_cont);
        cont += "<div class = 'case-cont'>"+
					"<div class='case-name'>"+data[i]['client']['title']+"</div>"+
					    "<div class='case-goods-cont'>"+
					        soln_cont+
			                "<div id = 'case-goods-"+i+"'></div>"+
			                "<div class='row center'><input type='text' placeholder='Solution' class='add-input case-goods-input id"+i+"' id = 'case-goods-input-"+i+"'></div>"+
			                "<div class='add-btn' id='add-issue-add-btn' onclick = 'case_send_goods("+i+")'>+Add Solution</div>"+
			            "</div>"+
                    "<div class='row center'>"+
                        "<div class='add-btn case_btn case_btn_view sg6 u_id"+data[i]['client']['u_id']+"'>View</div>"  + 
                        "<div class='add-btn case_btn case_btn_submitsoln sg6 id"+i+" u_id"+data[i]['client']['u_id']+"'>Submit</div>"  + 
					    "<div class='add-btn case_btn case_btn_remove sg6 u_id"+data[i]['client']['u_id']+"'>Remove("+data[i]['remove'].length+")</div>"+
                    "</div>"+
				"</div>";
	}
	targ.innerHTML = "<div id ='cv'></div>"+"<div id='cir-card' class='card cd-cases'><div class = 'cd-title'>Help Given</div>"+"<div id ='cv'></div>"+ cont +'</div>';
	case_listener();
	$('.case-soln').change(function(e){
		var cl = $('#lb-'+this.id).attr('class');
		var cla = cl.substr(0, cl.indexOf(' '));
		var cls = '';
		if($(this).is(':checked')){
			if(cl.indexOf('added') != -1){
				cls = cla+' cv-added';
			}else{
				cls = cla+' cv-checked';
			}
		    
		}else{
			if(cl.indexOf('added') != -1){
				cls = cla+ ' cv-added-unchecked';
			}else{
				cls = cla+' cv-unchecked';
			}
			
		}
		setAttributes(document.getElementById('lb-'+this.id), {'class':cls});
	});
}
ca[7] = function(data){
	var targ = document.getElementById('cases-main');
	var cont = "";

	for(var i = 0; i<data.length; i++){
		var success_cont ="";
		var failure_cont = "";
         var overview = '';
		if(data[i]['client']['report'] != ''){

			var report = JSON.parse(data[i]['client']['report']);
			for (var j =0;j<Object.keys(report['success']).length;j++){

	            success_cont += 
	                        "<div id='success-cont"+add_issue_count+"-cont' class = 'row center'>"+
						        "<div class='success-cont"+"-cont case-list case-list-"+i+"' id='success-cont"+add_issue_count+"'>"+report['success'][Object.keys(report['success'])[j]]+"</div>"+
						        "<div class = 'case-report-btn add-btn' onclick=\"case_remove_list('success-cont"+add_issue_count+"-cont')\">-</div>"+
						    "</div>";
			}
			for (var j =0;j<Object.keys(report['failure']).length;j++){
				
	            failure_cont += 
	                        "<div id='failure-cont"+add_issue_count+"-cont' class = 'row center'>"+
						        "<div class='failure-cont"+"-cont case-list case-list-"+i+"' id='failure-cont"+add_issue_count+"'>"+report['failure'][Object.keys(report['failure'])[j]]+"</div>"+
						        "<div class = 'case-report-btn add-btn' onclick=\"case_remove_list('failure-cont"+add_issue_count+"-cont')\">-</div>"+
						    "</div>";
			}
			overview = report['overview'];
		}
		
        cont += 
                "<div class = 'case-cont'>"+
					"<div class='case-name'>"+data[i]['client']['title']+"</div>"+
					"<div class= 'row center'><textarea type='text' class='add-input' placeholder='Overview' id='report-overview-"+i+"'>"+overview+"</textarea></div>"+
					

					"<div class='row case-box'>"+
						"<div id = 'case-success'>"+
						success_cont+
						"</div>"+
						"<input type='text' class='add-input' id='case-success-input-"+i+"' placeholder='Success'>"+
						"<div class='add-btn case-report-add' onclick=\"case_send_list('case-success','"+i+"')\">+</div>"+
					"</div>"+


					"<div class='row case-box'>"+
						"<div id = 'case-failure'>"+
						failure_cont+
						"</div>"+
						"<input type='text' class='add-input' id='case-failure-input-"+i+"' placeholder='Failure'>"+
						"<div class='add-btn case-report-add' onclick=\"case_send_list('case-failure','"+i+"')\">+</div>"+
					"</div>"+


                    "<div class='row center'>"+
					    "<div class='add-btn case_btn case_btn_view sg7 u_id"+data[i]['client']['u_id']+"'>View</div>"  +  
					    "<div class='add-btn case_btn case_btn_validate sg7 u_id"+data[i]['client']['u_id']+"'>Validate("+data[i]['validate_7'].length+")</div>"  +  
					    "<div class='add-btn case_btn case_btn_rptsubmit id"+i+" u_id"+data[i]['client']['u_id']+"'>Submit</div>"+
                    "</div>"+
				"</div>";
	}	
	targ.innerHTML = "<div id ='cv'></div>"+"<div id='cir-card' class='card cd-cases'><div class = 'cd-title'>Report</div>"+ cont + '</div>';
	case_listener();
}
/*-------------------------------------ca[4]-----------------------------------------*/
function case_send_goods(id){
    var el = document.getElementById('case-goods-input-'+id);
    var val = el.value;
    if(val != ""){
        add_issue_count += 1;
        document.getElementById('case-goods-'+id).innerHTML += "<div class = 'row center' id = 'case-goods-"+add_issue_count+"'><div class = 'case-goods ca-soln-"+id+" cv-added'>"+val+"</div><div class = 'add-btn' onclick = \"case_goods_remove(\'case-goods-"+add_issue_count+"\')\"> - </div></div>";
        el.value ='';
        $('.case-goods-input-'+id).focus();
    }else{
        notif('No text in the field');
    }
}
function case_goods_remove(el){
    rm_el(document.getElementById(el));
}
function case_submit_soln(case_id, id, stage){
    
    online(function(){
    	var els = document.getElementsByClassName('ca-soln-'+id);
		var solution = {};
		for(var i=0; i<els.length; i++){
			solution[i]={'solution':{}, 'state':{}};
			solution[i]['solution'] = els[i].innerHTML;
			solution[i]['state'] = els[i].className.substr(els[i].className.indexOf('cv-') + 3);
		}
		console.log(solution);

		$.ajax({
	        type:'POST',
	        url:'http://www.roh.nurmaan.free-staging-server.us/api/solution',
	        dataType:'json',
	        data:{case_id:case_id, user_id:user_id, stage:stage, solution:JSON.stringify(solution)},
	        success:function(data){
	        	display('circle-'+stage);
                if(data.result){
                    notif('Operation success');
                }else{
                	if(data.code == 7){
                		//notif('You have already validated this case');
                	}else{
                	    notif('Failed to load data. Please try again');
                	}
                }

	        },
	        error:function(data){
	            notif('Failed to load data. Please try again');
	        }
	    });
	}, function(){
		notif('Sorry, could not process your request because you are offline');
	});
	
}

function case_pub_hlp(case_id){

    $('.cover#pubhlp').css({'display':'unset'});
    $('.cover#pubhlp').animate({'opacity':'1'}, 500);
    document.getElementById('pubhlp-case_id').value = case_id;

    $('#pubhlp-x').click(function(e){
    	$('.cover#pubhlp').css({'display':'none', 'opacity':'0'});
    });
}

function pubhlp_confirm(){
	var data = {};
	var case_id = document.getElementById('pubhlp-case_id').value;
	if(document.getElementById('pubhlp-desc').value != ''){
		data['description'] = document.getElementById('pubhlp-desc').value;
		data['photo'] = document.getElementById('pubhlp-photo').checked;
		data['situation'] = document.getElementById('pubhlp-situation').checked;

		console.log(data);
		console.log(JSON.stringify(data));

		online(function(){
			$.ajax({
		        type:'POST',
		        url:'http://www.roh.nurmaan.free-staging-server.us/api/add_normal_case',
		        dataType:'json',
		        data:{user_id:user_id, case_id:case_id, data:JSON.stringify(data)},
		        success:function(data){
	                if(data.result){
	                    notif('Case made public');
	                    $('.cover#pubhlp').css({'display':'none'});
                        $('.cover#pubhlp').animate({'opacity':'0'}, 500);

	                }else{
	                	notif('Failed to load data. Please try again');
	                }

		        },
		        error:function(data){
		            notif('Failed to load data. Please try again');
		        }
		    });
		}, function(){
			notif('Sorry, could not process your request because you are offline');
		});
	}else{
		notif('An overview needs to be provided');
	}
	
        
	
}

function help(case_id){	
	if(document.getElementById('help').value != ''){
		var data = document.getElementById('help').value;
		online(function(){
		$.ajax({
	        type:'POST',
	        url:'http://www.roh.nurmaan.free-staging-server.us/api/help',
	        dataType:'json',
	        data:{user_id:user_id, case_id:case_id, data:data},
	        success:function(data){
                if(data.result){
                    show_msg('Thank You 😃', 'We highly value your help and endevour towards making our contry a better place. We will keep in touch via email.', {0:{'name':'Im glad to help', 'callback':nullHandler}});

                }else{
                	notif('Failed to load data. Please try again');
                }

	        },
	        error:function(data){
	            notif('Failed to load data. Please try again');
	        }
		    });
		}, function(){
			notif('Sorry, could not process your request because you are offline');
		});
	}else{
		notif('You cannot send an empty message');
	}
}

/*-----------------------------------ca[7]--------------------------------------------*/
function case_send_list(elem, id){
    var el = document.getElementById(elem);
    var valel =  document.getElementById(elem+'-input-'+id);
    var val = valel.value;
    if(val != ''){
    	add_issue_count +=1;
    	valel.value = '';
        el.innerHTML += "<div id='"+elem+add_issue_count+"-cont' class = 'row center'>"+
					        "<div class='"+elem+"-cont case-list case-list-"+id+"' id='"+elem+add_issue_count+"'>"+val+"</div>"+
					        "<div class = 'case-report-btn add-btn' onclick=\"case_remove_list('"+elem+add_issue_count+"-cont')\">-</div>"+
					    "</div>";
    }else{
    	notif('No text in the field');
    }
    $('#'+elem+'-input-'+id).focus();
   
}
function case_remove_list(el){
    rm_el(document.getElementById(el));
}
function case_submit_report(case_id, id, stage){
    online(function(){
    	var els = document.getElementsByClassName('case-list-'+id);
		var report = {'success':{},'failure':{},'overview':''};
		console.log(els);
		for(var i=0; i<els.length; i++){
			var cl = els[i].className;
			console.log(els[i].innerHTML);
			if(cl.indexOf('success') != -1){
                report['success'][i] = els[i].innerHTML;
			}else{
				report['failure'][i] = els[i].innerHTML;
			}
		}
		report['overview'] = document.getElementById('report-overview-'+id).value;
		console.log(report);

		$.ajax({
	        type:'POST',
	        url:'http://www.roh.nurmaan.free-staging-server.us/api/report',
	        dataType:'json',
	        data:{case_id:case_id, user_id:user_id, stage:stage, report:JSON.stringify(report)},
	        success:function(data){
                if(data.result){
                    notif('Operation success');
                }else{
                	if(data.code == 7){
                		//notif('You have already validated this case');
                	}else{
                	    notif('Failed to load data. Please try again');
                	}
                }

	        },
	        error:function(data){
	            notif('Failed to load data. Please try again');
	        }
	    });
	}, function(){
		notif('Sorry, could not process your request because you are offline');
	});	
}
/*-----------------------------------Generals-----------------------------------------*/
function case_listener(){
	$('.case_btn').click(function(e){
    	console.log(this);
    	console.log($(this).attr("class"));
    	var cl = $(this).attr("class");
    	cla = cl.substr(cl.indexOf('u_id') + 4);
    	if(cl.indexOf('view') != -1){
    		case_view(cla, cl.substr(cl.indexOf('sg') + 2,1));
    	}else if(cl.indexOf('validate') != -1){
    		case_validate(cla, cl.substr(cl.indexOf('sg') + 2,1));
    	}else if(cl.indexOf('remove') != -1){
    		case_remove(cla, cl.substr(cl.indexOf('sg') + 2,1));
    	}else if(cl.indexOf('schedule') != -1){
  			case_schedule(cla, cl.substr(cl.indexOf('id') + 2,1));
 
            
    	}else if(cl.indexOf('submitsoln') != -1){
    		case_submit_soln(cla, cl.substr(cl.indexOf('id') + 2,1), cl.substr(cl.indexOf('sg') + 2,1));

    	}else if(cl.indexOf('rptsubmit') != -1){
    		case_submit_report(cla, cl.substr(cl.indexOf('id') + 2,1), cl.substr(cl.indexOf('sg') + 2,1));

    	}else if(cl.indexOf('pubhlp') != -1){
    		case_pub_hlp(cla, cl.substr(cl.indexOf('id') + 2,1));

    	}
    });
}

function case_validate(cl, stage){
    var u_id = cl;
    console.log(u_id);
    online(function(){
		$.ajax({
	        type:'POST',
	        url:'http://www.roh.nurmaan.free-staging-server.us/api/validate',
	        dataType:'json',
	        data:{id:u_id, u_id:user_id, stage:stage},
	        success:function(data){
	        	display('circle-'+stage);
                if(data.result){

                }else{
                	if(data.code == 7){
                		notif('You have already validated this case');
                	}else{
                	    notif('Failed to load data. Please try again');
                	}
                }

	        },
	        error:function(data){
	            notif('Failed to load data. Please try again');
	        }
	    });
	}, function(){
		notif('Sorry, could not process your request because you are offline');
	});
}

function case_remove(cl, stage){
    var u_id = cl;
    console.log(u_id);
    online(function(){
		$.ajax({
	        type:'POST',
	        url:'http://www.roh.nurmaan.free-staging-server.us/api/remove',
	        dataType:'json',
	        data:{id:u_id, u_id:user_id, stage:stage},
	        success:function(data){
	        	display('circle-'+stage);
                if(data.result){

                }else{
                	if(data.code == 7){
                		notif('You have already removed this case');
                	}else{
                	    notif('Failed to load data. Please try again');
                	}
                }

	        },
	        error:function(data){
	            notif('Failed to load data. Please try again');
	        }
	    });
	}, function(){
		notif('Sorry, could not process your request because you are offline');
	});
}

function case_schedule(case_id, id){
	console.log(id);

	var c_id = case_id+'sg';

    var coordinates = case_data[id]['client']['coordinates'] || "unset";
    console.log('coordinates:  '+coordinates);
	console.log(get_pln_add());
	show_pln_add({0:{'elem':'pln-title', 'value':case_data[id]['client']['title']}, 2:{'elem':'lbl-pln-getloc', 'value':coordinates}, 1:{'elem':'pln-aud-pri','value':'private'}, 3:{'elem':'pln-case_id', 'value':c_id}, 4:{'elem':'stage'}});
}

function init_case_listener(){
    $('.circle').click(function(e){
    	console.log(this.id);
    	display(this.id);

    });

    $('#cv-x').click(function(e){
    	hide_cv();
    });	
}

function show_normal(){
	$('#admin-panel').css({'display':'none'});
    $('#normal-panel').css({'display':'unset'});
    normal_view();
    
}
function show_admin(){
	$('#admin-panel').css({'display':'unset'});
    $('#normal-panel').css({'display':'none'});
}