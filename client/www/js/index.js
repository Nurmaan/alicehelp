var func = [];
var pic = [];
var pic_id =[];
var pic_id_assoc = {};
var win_wid;
var win_hei;
var notif_st = false;
var notif_t; 
var acc_work;
var app = {
    initialize: function() {
        this.bindEvents();
    },  
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function(){
        deviceReady();
    }
      
};
var func_temp = [];
var member=1;
var region_t = 1000;
var db;
var user_id = '';
var temp_t = 2000;
var temp_pic_t = 2000;
var cases_title = {0:'Preliminary Screening', 1:'Schedule 1st Visit', 2:'Validate Case', 3:'Means', 4:'Schedule 2nd Visit', 5:'Validate Completion', 6:'Last Visit', 7:'Report'};
var ca = [];
var add_issue_count = 0;
var add_solution_count = 0;
var deviceType;
var display_planner = [];

function deviceReady(){


 /*   show_msg('Notice 😥','This app is a beta version. Please make sure to download the official app from Google Playstore and App Store as from July 2018.',{0:{'name':'Yeah i get it 😊', 'callback':function(){
        hide_msg();
    }}});*/


    deviceType = (navigator.userAgent.match(/iPad/i))  == "iPad" ? "iPad" : (navigator.userAgent.match(/iPhone/i))  == "iPhone" ? "iPhone" : (navigator.userAgent.match(/Android/i)) == "Android" ? "Android" : (navigator.userAgent.match(/BlackBerry/i)) == "BlackBerry" ? "BlackBerry" : "null";
    disp_over("Device Type: "+deviceType);

    download('view/account.html',func['account']);

    db = window.openDatabase('roh', '1.0', 'Rays Of Hope', 200000);
    db.transaction(function(tx){
        //tx.executeSql('DROP TABLE IF EXISTS account');
        //tx.executeSql('DROP TABLE IF EXISTS pics');
        tx.executeSql( 'CREATE TABLE IF NOT EXISTS region(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, date TEXT NOT NULL)');
        tx.executeSql('CREATE TABLE IF NOT EXISTS account (id INTEGER PRIMARY KEY AUTOINCREMENT, email VARCHAR(25), first_name VARCHAR(25), last_name VARCHAR(25), age VARCHAR(3), mobile VARCHAR(15), occupation VARCHAR(25), region VARCHAR(25), address VARCHAR(25), bio VARCHAR(25), pic VARCHAR(25), u_id VARCHAR(25), validated VARCHAR(3), type VARCHAR(7))', [],nullHandler,errorHandler);        //tx.executeSql( 'CREATE TABLE IF NOT EXISTS adcas(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, data TEXT NOT NULL, date TEXT NOT NULL)',[],nullHandler,errorHandler);
        tx.executeSql( 'CREATE TABLE IF NOT EXISTS pics(id INTEGER PRIMARY KEY AUTOINCREMENT, data TEXT)');
        tx.executeSql('CREATE TABLE IF NOT EXISTS temp (id INTEGER PRIMARY KEY AUTOINCREMENT, data TEXT, url TEXT, success TEXT, server_error TEXT, connection_error TEXT)');
/*        tx.executeSql('INSERT INTO temp (data) VALUES ("asdfasfs")');
        tx.executeSql('SELECT * FROM temp;', [], function(tx,result){console.log(result);},errorHandler);*/
    }, errorCB, function(){
        //check_temp();
    });
/*
 db.transaction(function(tx){
        console.log('starting...');
        tx.executeSql('DROP TABLE IF EXISTS user');
        tx.executeSql('CREATE TABLE IF NOT EXISTS user (id INTEGER PRIMARY KEY AUTOINCREMENT, data VARCHAR(25))');        
    }, errorCB, snowit);*/






    win_wid = document.getElementById('background').offsetWidth;
    win_hei = document.getElementById('background').offsetHeight;
    //show_db('account',['id']);
    check_user();

    //setTimeout(function(){notif('Hello :D');}, 2000);

    $(window).bind('beforeunload', function(){
    });

    $('#adds').keyup(function(e){
        var el = document.getElementById('adds');
        var h = el.value.length;
        var dis_w = document.body.offsetWidth;
        var height = h/(dis_w/13) * 20;
        
        el.style.height = height + 'px';


    });



    $('#hamburger').click(function(e){
        show_sidebar();
    });
    $('#log').click(function(e){
        show_overlay('close');
    });
    $('#sidebar-x').click(function(e){
        hide_sidebar();
    });

    $('.side-cont').click(function(e){
        hide_sidebar();
        download("view/"+e.target.id+".html", func[e.target.id]);
    });
    $('#overlay-x').click(function(e){
        console.log('x');
        hide_overlay();
    });
    
}

function check_user_type(admin, normal){
    disp_over('checking user type...');
    db.transaction(function(tx){
        tx.executeSql('SELECT type FROM account WHERE u_id = "'+user_id+'";', [],
            function(tx, result) {
                console.log(result.rows.length);
                if (result != null && result.rows.length != 0) {
                    if(result.rows[0]['type'] == 1){
                        disp_over('Admin user');
                        admin();
                    }else{
                        disp_over('Normal user')
                        normal();
                    }
                }else{
                    disp_over('No account found. Need to login first...');
                    notif('You need to login first');
                    download('view/account.html', func['account']);
                }
            },function(error){disp_over('error 2: '+error.message)});
    }, errorCB, successCB);
}

function init_auto_height(){
    $('.auto_height').keyup(function(e){
        console.log(this.id);
        var el = document.getElementById(this.id);
        var h = el.value.length;
        var dis_w = document.body.offsetWidth;
        var height = h/(dis_w/13) * 20;
        
        el.style.height = height + 'px';


    });
}

function getLocation(successFunc, errorFunc, accuracy){
    if(errorFunc == 'null'){errorFunc = locationError;}
    navigator.geolocation.getCurrentPosition(successFunc, errorFunc, { enableHighAccuracy: accuracy });
}

function show_map(latitude, longitude, geocoords, lbl){
     
    if(latitude != "" && longitude != ""){
        geocoords = latitude + ',' + longitude;
    }
    
 
    if (deviceType == 'Android') {
        var label = encodeURI(lbl);
        window.open('geo:0,0?q=' + geocoords + '(' + label + ')', '_system');
     
    }
    else{
        window.open('maps://?q=' + geocoords, '_system');

    }
}
function locationSuccess(position){
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    disp_over(latitude);
    disp_over(longitude);

   /* var mapOptions = {
        center:new google.maps.LatLng(latitude, longitude),
        zoom:15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
        var map = new google.maps.Map(document.body, mapOptions);*/

}

function locationError(error){
   disp_over('code: '    + error.code    + '\n' + 'message: ' + error.message + '\n');
   if(error.code == 1){
        notif('Permission to access GPS has been denied. Operation failed');
   }else if(error.code == 2){
        notif('Cannot get your location. Please check that your GPS is turned on');
   }else if(error.code == 3){
        notif('GPS timed out. Please try again');
   }
}

function snowit(){
    db.transaction(function(tx){
        tx.executeSql('INSERT INTO user (data) VALUES ("Hey there this is just a test")', [], nullHandler, errorHandler);
        tx.executeSql('SELECT * FROM user;',[],function(tx,result){
            console.log(result);
        },errorHandler);
    }, errorCB, successCB);
}
function check_temp(){
    disp_over('Checking temporary data...')
    db.transaction(function(tx){
        tx.executeSql('SELECT * FROM temp;',[], function(tx,result){
            if(result != null && result.rows.length > 0){
                disp_over('Some data has not yet been uploaded...');
                for(var i = 0; i < result.rows.length; i++){
                    upload_temp(result.rows.item(i)['id'], result.rows.item(i)['data'], result.rows.item(i)['url'], result.rows.item(i)['success'], result.rows.item(i)['server_error'], result.rows.item(i)['connection_error']);
                }
            }else{
                disp_over('No temporary data found');
            }
        }, errorHandler);
    }, errorCB, successCB);
}
function upload_temp(id, raw_data, url, success, server_error, connection_error){
    disp_over('Uploading data...');
    var data = JSON.parse(raw_data);
    online(function(){
        $.ajax({
                type       : "POST",
                url        : url,
                crossDomain: true,
                beforeSend : function() {disp_over('Uploading data...');},
                complete   : function() {},
                data       : {data : data},
                dataType   : 'json',
                success    : function(data) {
                    if(data.result){
                        func_temp[success](data);
                        db.transaction(function(tx){
                            tx.executeSql("DELETE FROM temp where id = " + id,[],nullHandler,errorHandler);
                        }, errorCB, successCB);
                    }else{
                        func_temp[server_error](data);
                        temp_t += 200;
                        setTimeout(check_temp(), temp_t);
                        disp_over('There has been a server error. Trying again in '+(temp_t)/1000 +'s...');

                    }
                },
                error      : function() {
                    func_temp[connection_error](data);
                    disp_over('There has been a connection error. Trying again in '+(temp_t)/1000 +'s...');
                    temp_t += 200;
                    setTimeout(check_temp(), temp_t);
                }
            });
    }, function(){
        temp_t += 2000;
        disp_over('No internet connection. Trying again in '+(temp_t)/1000 +'s...');
        setTimeout(check_temp(), temp_t);
    });
}
function temp_save(data, url, success, server_error, connection_error){
    db.transaction(function(tx){
        tx.executeSql("INSERT INTO temp (data, url, success, server_error, connection_error) VALUES ('"+data+"','"+url+"', '"+success+"', '"+server_error+"', '"+connection_error+"')");
    }, errorCB, successCB);
}
function check_user(){
    disp_over('Checking user id....');
    db.transaction(function(tx){
        tx.executeSql("SELECT * FROM account;",[],function(tx,result){
            console.log(result);
            if(result != null && result.rows.length > 0){
                
                user_id = result.rows.item(result.rows.length-1)['u_id'];
                disp_over('User id found...');
            }else{
                disp_over('Failed to find user id; a login is required...');
                download('view/account.html', func['account']);
            }
        }, errorHandler)
    }, errorCB, successCB);
}
/*function getImage(success, failure) {
    navigator.camera.getPicture(success, failure, {
        destinationType: navigator.camera.DestinationType.FILE_URI,
        sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY

    });
}*/


function download(url, callback){
        disp_over('Changing screen...');
        var request = new XMLHttpRequest();
        request.open("GET", url);
        request.onreadystatechange = function() {
            if (request.readyState == 4) {
                document.getElementById('app').innerHTML = request.responseText;
                callback(request.responseText);
            }
        }
        request.send();
}

function show_db(name, values){
    disp_over('attempting to display database ' + name + '...');
    db.transaction(function(tx){
        tx.executeSql('SELECT * FROM region;',[], function(tx, result){
            disp_over('success loading data...');
            console.log(result);
            if(result != null && result.rows.length != 0){
                for(var i = 0; i <result.rows.length; i++){
                    var val="";
                    for(var j=0; j<values.length;j++){
                        var ind = values[j];
                        val+="  "+result.item(i)[ind];
                    }
                    disp_over(val);
                }
            }
        }, errorHandler);
    }, errorCB, successCB);

}

function show_sidebar(){
    $('#sidebar').css({'display':'unset'});
    $('#sidebar').animate({'height':'100vh','width':'100vw', 'top':'0%', 'left':'0%'}, 500);
    setTimeout(function(){
        $('#sidebar-x').css({'display':'unset'});
        $('#sidebar-x').animate({'padding':'0.25em', 'margin':'0.5em'});
        $('.side-cont').css({'display':'unset'});
    }, 300);

}

function hide_sidebar(){
    $('#sidebar-x').animate({'padding':'0em', 'margin':'0em'}, 250);
    $('#sidebar').animate({'height':'0vh','width':'0vw', 'top':'50%', 'left':'50%'}, 500);
    $('.side-cont').css({'display':'none'});
    setTimeout(function(){
        $('#sidebar').css({'display':'none','left':'0%'}); 
    }, 500);
    setTimeout(function(){
        $('#sidebar-x').css({'display':'none'});     
    }, 300);
}

function rm_el(el){
    el.parentNode.removeChild(el);
};

function hide_navbar(){
    $('#nav-hidden').css({'display':'unset'});
    $('#navbar').animate({'width':'3em'}, 700);
    setTimeout(function(){
        $('.nav-comp-normal').css({'display':'none','margin':'none'});
        $('#nav-hidden').css({'display':'unset'});
        $('#navbar').css({ 
            'padding-right':'0vw',
            'padding-left':'0vw',
            'padding-top':'0vh'});
    }, 600);
    
}
function show_navbar(){
    $('#nav-hidden').css({'display':'none'});
    $('#navbar').animate({'width':'100%'}, 700);
    $('.nav-comp-normal').css({'display':'unset'});
    $('#nav-hidden').css({'display':'none'});
    $('#navbar').css({ 
        'padding-right':'1vw',
        'padding-left':'1vw',
        'padding-top':'1vh'});
}

function resize(){
    //alert('resize');
    align_dropdown();
    if(document.getElementById('background').offsetHeight < win_hei || document.getElementById('background').offsetWidth < win_wid){
        hide_navbar();
    }else{
        show_navbar();
    }
}

function setAttributes(el, attrs) {
  for(var key in attrs) {
    el.setAttribute(key, attrs[key]);
  }
}

function notif(msg){
    if(!notif_st){
        notif_st = true;
        document.getElementById('notif').innerHTML = msg;
        $('#notif').css({'display':'unset'});
        $('#notif').animate({'top':'0px'}, 500).animate({'top':'-10px'}, 500).animate({'top':'0px'}, 250);
        notif_t = setTimeout(function(){
            $('#notif').animate({'top':'5px'}, 250).animate({'top':'-8em'}, 400);
            notif_st = false;
        }, 7000);
    }else{
        clearTimeout(notif_t);
        $('#notif').animate({'top':'5px'}, 250).animate({'top':'-8em'}, 400);
        setTimeout(function(){
            document.getElementById('notif').innerHTML = msg;
            $('#notif').css({'display':'unset'});
            $('#notif').animate({'top':'0px'}, 500).animate({'top':'-10px'}, 500).animate({'top':'0px'}, 250);
            notif_t = setTimeout(function(){
                $('#notif').animate({'top':'10px'}, 300).animate({'top':'-8em'}, 400);
                notif_st = false;
            }, 7000);
        }, 700);
    }
    
}
function online(on, off){
    if(window.navigator.onLine){
        $.ajax({
            type:'POST',
            dataType: 'json',
            url:'http://www.roh.nurmaan.free-staging-server.us/api/online',
            success:function(data){
                if(data){
                    on();
                }else{
                    console.log('Error occured while connecting to the internet... data: '+data);
                    off('server error');
                }
            },
            error:function(data){
                off('server offline');
            }
        });
    }else{
        off('device offline');
    }
    
}
function disp_over(msg){
    console.log(msg);
    document.getElementById('overlay').innerHTML += msg + "<br/>";
    $('#overlay').animate({scrollTop:$('#overlay').height()}, 'slow');
}
function show_overlay(state){
    if(state == 'close'){
        $('#overlay-x').css({'display':'unset'});
    }

    $('#overlay').css({'display':'unset'});
}
function hide_overlay(){
   $('#overlay').css({'display':'none'});
   $('#overlay-x').css({'display':'none'}); 
}
function nullHandler(){
}
function errorHandler(error){
    console.log(error.message);
}
function errorCB(err) {
    disp_over("Error processing SQL: "+err.code);
}

function successCB() {
    disp_over("success processing database!");
}



function generate(len){

    var num =  (Math.random() * 10000) + 1000;
    var num_2 = num * ((Math.random() * 9788) + 657);
    var ret = num_2.toString(25) + num_2.toString(36) + num_2.toString(30) + num_2.toString(33);
    var ret_2 = ret.replace(/\./g, 'm');
    return ret_2.slice(-len);
}

function power(base, power){
    var result=1;
    for(var i = 0; i < power; i++){
        result *= base;
    }
    return result;
}
/*---------------------- msg and option --------------------------*/
function show_msg(title, msg, buttons){
    /*buttons in the format {0:{'name':'name','callback':callback()},....}*/
    var btn ="";
    console.log(buttons);
    document.getElementById('msg-btns').innerHTML = "";
    document.getElementById('msg-msg').innerHTML = msg;
    document.getElementById('msg-title').innerHTML = title;
    for (var i = 0; i<Object.keys(buttons).length; i++) {
        btn += "<div class = 'msg-btn btn' id='msg-btn-"+i+"'>"+buttons[i]['name']+"</div>";
    }
    document.getElementById('msg-btns').innerHTML += btn;
    $('.cover#msg').css({'display':'unset'});
    $('.cover#msg').animate({'opacity':'1'}, 1000);

    setTimeout(function(){
        $('#msg-container').css({'display': 'inline-block'});
        $('#msg-container').animate({'width':'65vw','left':'17.5vw'}, 500).animate({'width':'56vw','left':'22vw'}, 500).animate({'width':'60vw','left':'20vw'}, 500);
    },250);

    setTimeout(function(){
        
        $('#msg-msg').animate({'opacity':'1'}, 250);
        $('#msg-btns').animate({'opacity':'1'}, 250);
    }, 1750)

    setTimeout(function(){
        console.log('initialising listener');
        init_msg_listener(buttons);
    }, 2000);


}

function hide_msg(){
    $('.cover#msg').animate({'opacity':'0'}, 500);
    $('#msg-container').animate({'top':'70vh', 'height':'0vh'}, 750);

    setTimeout(function(){
        $('#msg-container').css({'top':'25vh','height':'unset','display':'none','width':'0vw', 'left':'0vw'});
        $('#msg-msg').css({'opacity':'0'});
        $('#msg-btns').css({'opacity':'0'});
        $('.cover#msg').css({'display':'none','opacity':'0'});
        disp_over('SUCCESS!!!!!!');
    }, 800);

    /*setTimeout(function(){
        show_msg('test','hey there welcome to this app',{0:{'name':'okay i get it', 'callback':function(){hide_msg()}}, 1:{'name':'no sorry', 'callback':function(){notif('You can\'t do that to me >:(')}}});
    }, 2000);*/
    
}

function init_msg_listener(buttons){
    $('.msg-btn').click(function(e){
        var id = this.id.substr(this.id.lastIndexOf('-') + 1);
        console.log('id: '+id);
        hide_msg();
        buttons[id]['callback']();
    });

    $('#msg-x').click(function(e){
        hide_msg();
    });
}
function show_option(title,options){
    var opt = '';
    document.getElementById('option-title').innerHTML = title;
    document.getElementById('options').innerHTML = '';
    console.log(options);

    for(var i =0; i<Object.keys(options).length; i++){
        opt+="<div class='row center'><div class='option btn' id='option-"+i+"''>"+options[i]['name']+"</div></div>";
    }

    document.getElementById('options').innerHTML = opt;
    $('.cover#option').css({'display':'unset'});
    $('.cover#option').animate({'opacity':'1'}, 1000);

    setTimeout(function(){
        $('#option-container').css({'display': 'inline-block'});
        $('#option-container').animate({'width':'65vw','left':'17.5vw'}, 500).animate({'width':'56vw','left':'22vw'}, 500).animate({'width':'60vw','left':'20vw'}, 500);
    }, 250);

    setTimeout(function(){
        $('.option').animate({'opacity':'1'}, 250);
    }, 1750);

    setTimeout(function(){
        console.log('initialising listener');
        init_opt_listener(options);
    }, 2000);
}

function hide_option(){
    $('.cover#option').animate({'opacity':'0'}, 500);
    $('#option-container').animate({'top':'70vh', 'height':'0vh'}, 750);

    setTimeout(function(){
        $('#option-container').css({'top':'25vh','height':'unset','display':'none','width':'0vw', 'left':'50vw'});
        $('.option').css({'opacity':'0'});
        $('.cover#option').css({'display':'none','opacity':'0'});
        disp_over('SUCCESS!!!!!!');
    }, 800);
}

function init_opt_listener(options){
    $('.option').click(function(e){
        var id = this.id.substr(this.id.lastIndexOf('-') + 1);
        console.log(id);
        options[id]['callback']();
    });

    $('#option-x').click(function(e){
        hide_option();
    });
}
/*-------------------------Image-----------------------------------*/
function getImage(success, error){
    show_option('Where to?', {
        0:{'name':'Camera(Recommended)','callback':function(){
                console.log(error);
    console.log(success);
            requestImage(Camera.PictureSourceType.CAMERA, success, error);
        }}, 
        1:{'name':'Gallery','callback':function(){
            requestImage(Camera.PictureSourceType.PHOTOLIBRARY, success, error);
        }
        }
    });
}

function requestImage(srcType, success, error){
    var options = {
    // Some common settings are 20, 50, and 100
    quality: 30,
    destinationType: Camera.DestinationType.FILE_URI,
    // In this app, dynamically set the picture source, Camera or photo gallery
    sourceType: srcType,
    encodingType: Camera.EncodingType.JPEG,
    mediaType: Camera.MediaType.PICTURE,
    /*allowEdit: true,*/
    correctOrientation: true  //Corrects Android orientation quirks
    }
    console.log(error);
    console.log(success);
    navigator.camera.getPicture(function(imageURI){
        imageSuccess(imageURI);
        success(imageURI);
    }, function(msg){
        imageError(msg);
        error(msg);
    }, options);
}

function imageSuccess(){
    hide_option();
    notif('Successfully loaded photo');
    navigator.camera.cleanup();
}

function imageError(msg){
    hide_option();
    notif('Failed to load photo: '+msg);
    navigator.camera.cleanup();
}

/*function upload_img(imageURI, img_name, success, error){
    alert('uploading image...');
    var options = new FileUploadOptions();
    options.fileKey ="file";
    options.fileName = img_name;//imageURI.substr(imageURI.lastIndexOf('/')+1);
    options.mimeType ="image/jpeg ";
    options.chunkedMode = false;

    var ft = new FileTransfer();
    ft.upload(imageURI, "http://youngjournalist.nurmaan.free-staging-server.us/api/upload_img", function(r) {
        success(r);
    },
    function(error) {
        //document.getElementById('picture_msg').innerHTML = "Upload failed: Code = "+error.code;
        error(error);
    }, options);
}*/


