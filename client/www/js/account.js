func['account'] = function(){

	db.transaction(createDB, errorCB, show_db_account);
	if(user_id != ''){
        $('#account-edit').css({'display':'unset'});
        $('#account_ln_sn').css({'display':'none'});
        edit_account();
	}else{
		$('#account-edit').css({'display':'none'});
        $('#account_ln_sn').css({'display':'unset'});
	}

	$(document).click(function(e){
		console.log(e.target.id);
		if(e.target.id == 'login_btn'){
			AnimateRotate(0, 180, $('#signup'), 2000);
			AnimateRotate(180, 360, $('#login'), 2000);
			$('#signup').animate({'opacity':'0'}, 2000);
			$('#signup').css({'display':'none'});
			$('#login').css({'display':'unset'});
			$('#login').animate({'opacity':'1'}, 2000);
			$('#login').css({'z-index':'10'});

		}else if(e.target.id == 'signup_btn'){
			AnimateRotate(0, 180, $('#login'), 2000);
			AnimateRotate(180, 360, $('#signup'), 2000);
			$('#login').animate({'opacity':'0'}, 2000);
			$('#login').css({'display':'none'});
			$('#signup').css({'display':'unset'});
			$('#signup').animate({'opacity':'1'}, 2000);
			$('#login').css({'z-index':'-1'});

		}else if(e.target.id == 'overlay-x'){
			hide_overlay();
		}
		if(e.target.id != 'sn' && e.target.id != 'ln'){
			remove_alert();
		}
		
	});
}
/*--------------------EDIT ACCOUNT-----------------------------*/

function edit_account(){
    online(function(){
		$.ajax({
		    type       : "POST",
		    url        : "http://www.roh.nurmaan.free-staging-server.us/api/user",
		    crossDomain: true,
		    data       : {data : vars},
		    dataType   : 'json',
		    success    : function(data) {
		        if(data.result){
				    display_account_edit(data.data);
				}else{
					notif('An error occured. Please try again.');
				}
		    },
		    error      : function() {
		    	notif('An error occured.');
		    }

		});
	}, function(){
		disp_over('Fetching user data offline');
		db.transaction(function(tx){
			tx.executeSql('SELECT * FROM account;',[], function(tx, result){
				console.log(result);
			}, function(){notif('Could not fetch data offline. Please to conenct to the internet or report to an administrator');});
		}, errorCB, successCB);
	});
}
/*-------------------------------------------------------------*/

function AnimateRotate(start, end, elem, time) {

    $({deg: start}).animate({deg: end}, {
        duration: time,
        step: function(now) {
      
            elem.css({
                transform: 'rotate(' + now + 'deg)'
            });
        }
    });
}

function remove_alert(){
    var el = document.getElementsByClassName('acc-alert');
    console.log(el);
    for(var i =0; i< el.length; i++){
        el[i].parentNode.removeChild(el[i]);
    }
}

func_temp['ln_sn_success'] = function(data){

	notif('Welcome '+data.data.first_name);
	acc_save(data.data);
};

func_temp['ln_sn_server_error'] = function(data){
	if(data.code == 1){
		notif('This email address has already been registered. Please login instead');
		AnimateRotate(0, 180, $('#signup'), 2000);
		AnimateRotate(180, 360, $('#login'), 2000);
		$('#signup').animate({'opacity':'0'}, 2000);
		$('#login').animate({'opacity':'1'}, 2000);
		$('#login').css({'z-index':'10'});
		document.getElementById('ln-em').value = document.getElementById('sn-em').value;

	}else{
		notif(data.error);
	}
};

func_temp['ln_sn_conenction_error'] = function(data){
	notif('Failed... We will keep trying');
	temp_save(vars, "http://www.roh.nurmaan.free-staging-server.us/api/"+state, func_temp['ln_sn_success'], func_temp['ln_sn_server_error'], func_temp['ln_sn_connection_error']); 
};
function ln_sn(state){
	if(!acc_work){
	    if(check_(state)){
	    	console.log('state correct');
	    	var vars = data_(state,"");
	        //acc_save(data_(state,'raw'));
	    	console.log('vars: ' + vars);
	    	online(function(){
	    		$.ajax({
				    type       : "POST",
				    url        : "http://www.roh.nurmaan.free-staging-server.us/api/"+state,
				    crossDomain: true,
				    beforeSend : function() {notif('Processing your request...'); acc_work = true;},
				    complete   : function() {acc_work = false;},
				    data       : {data : vars},
				    dataType   : 'json',
				    success    : function(data) {
				    	acc_work = false;
				        if(data.result){
							notif('Welcome '+data.data.first_name);
							user_id = data.data['u_id'];
							acc_save(data.data);
							download('view/home.html', func['home']);
						}else{
							if(data.code == 1){
								notif('This email address has already been registered. Please login instead');
								AnimateRotate(0, 180, $('#signup'), 2000);
								AnimateRotate(180, 360, $('#login'), 2000);
								$('#signup').animate({'opacity':'0'}, 2000);
								$('#login').animate({'opacity':'1'}, 2000);
								$('#login').css({'z-index':'10'});
								document.getElementById('ln-em').value = document.getElementById('sn-em').value;

							}else{
								notif(data.error);
							}
							
						}
				    },
				    error      : function() {
                        acc_work = false;
				        notif('Failed... We will keep trying');
				        temp_save(vars, "http://www.roh.nurmaan.free-staging-server.us/api/"+state, func_temp['ln_sn_success'], func_temp['ln_sn_server_error'], func_temp['ln_sn_connection_error']);                
				    }
				});
	    	}, function(){});
	    	
	    }else{
	    	console.log('check state failed');
	    }
    }else{
    	notif('Task already in progress. Please wait');
    }
}

function data_(elem, type){
    var data={};
    var el = document.getElementsByClassName(elem);
    for(var i=0; i<el.length; i++){
        data[el[i].id] = el[i].value;
    }
    console.log(data);
    if(type == 'raw'){
    	return data;
    }else{
    	return JSON.stringify(data);
    }

}

function check_(elem){
	var state = true;
	var el = document.getElementsByClassName(elem);
	console.log(el);
	for(var i=0; i<el.length; i++){
		if(el[i].id != 'sn-mn'){
            if(el[i].value == '' || el[i].value == null){
            	alert_('This Field Is Mandatory', 'rw-'+el[i].id);
            	state = false;
            }
		}
	}
	var em = document.getElementsByClassName(elem+'-em')[0].value;
	var el;
	if(state == 'login'){
         el = 'ln';
	}else{
		el ='sn';
	}
	if(em != "" && !check_email(em, elem)){
		alert_('Incorrect Email Address', 'rw-'+el+'-em');
        state = false;
	}else{
		if(elem == 'signup' && !check_pswd(elem)){
          state = false;
		}
	}

	return state;
}

function check_pswd(state){
	if(state == 'signup'){
		if(document.getElementById('sn-pswd').value == document.getElementById('sn-pswd-c').value){
			return true;
		}else{
			alert_("The passwords do not match", 'rw-sn-pswd');
		}
	}
}
function check_mobile(el){

	//To find a better algo...

	if(el < 100000000 && el > 50000000){
         return true;
	}else{
		return false;
	}
}

function alert_(msg, el){
	console.log(el);
	var child = document.createElement('div'); setAttributes(child, {'class':'acc-alert'}); child.innerHTML = msg;
    document.getElementById(el).appendChild(child);
}

function warn_(msg, el){
	var child = document.createElement('div'); setAttributes(child, {'class':'acc-warn'}); child.innerHTML = msg;
    document.getElementById(el).appendChild(child);
}

function check_email(email){
    var one = email.indexOf('@');
    var two = email.lastIndexOf('.');
    if(one != -1 && two != -1){
        if(one<two){
        	return true;
        }else{
        	notif('Incorrect email address');
        	return false;
        }
    }else{
    	notif('Incorrect email address: missing \'@\' or \'.\'');
    	return false;
    }
}

function createDB(tx){
	//tx.executeSql('DROP TABLE IF EXISTS account');
	tx.executeSql('CREATE TABLE IF NOT EXISTS account (id INTEGER PRIMARY KEY AUTOINCREMENT, email VARCHAR(25), first_name VARCHAR(25), last_name VARCHAR(25), age VARCHAR(3), mobile VARCHAR(15), occupation VARCHAR(25), region VARCHAR(25), address VARCHAR(25), bio VARCHAR(25), pic VARCHAR(25), u_id VARCHAR(25), validated VARCHAR(3), type VARCHAR(7))', [],nullHandler,errorHandler);
	tx.executeSql('CREATE TABLE IF NOT EXISTS accTemp (id INTEGER PRIMARY KEY AUTOINCREMENT, email VARCHAR(25), first_name VARCHAR(25), last_name VARCHAR(25), age VARCHAR(3), mobile VARCHAR(15), occupation VARCHAR(25), region VARCHAR(25), address VARCHAR(25), bio VARCHAR(25), pic VARCHAR(25), u_id VARCHAR(25), validated VARCHAR(3))', [],nullHandler,errorHandler);
}

function acc_save(data){
    //var data = JSON.parse(raw_data);
    disp_over('Saving data...');
    console.log(data)
    console.log(data['first_name']);
    db.transaction(function(tx){
    	tx.executeSql(
    		    "INSERT INTO account (email, first_name,last_name, age, mobile, occupation, region, address, bio, u_id, type) VALUES ('"+data['email']+"','"+data['first_name']+"','"+data['last_name']+"','"+data['age']+"','"+data['mobile']+"','"+data['occupation']+"','"+data['region']+"','"+data['address']+"','"+data['bio']+"', '"+data.u_id+"', '"+data['type']+"');", 
    		    [],
    		    nullHandler,
    		    errorHandler);
    }, errorCB, show_db_account);
}

function show_db_account(){
	disp_over("Attempting to display database | account...")
    db.transaction(function(tx) {
        tx.executeSql('SELECT * FROM account', [],
            function(tx, result) {
                if (result != null && result.rows != null) {
                	console.log(result);
                	disp_over('success loading data...');
                    for (var i = 0; i < result.rows.length; i++) {
                        var row = result.rows[i];
                        disp_over(row['id'] + ' | ' +
                        row['first_name']+ ' | ' + row['last_name']+ ' | ' + row['email']+ ' | ' + row['u_id']+ ' | ' + row['bio']+ ' | ' + row['address']);
                    }
                }
            },function(error){disp_over('error 2: '+error)});
 },errorCB,function(){
    disp_over('Display succeeded');

 });
}