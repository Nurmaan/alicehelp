var add_late_time = 500;

func['add'] = function (){

    
    db.transaction(createDB, errorCB, successCB);

/*    if(user_id == null || user_id == ""){
        notif('You need to sign up or login first before you can report a case');
        download('../view/account.html',func['account']);
    }*/

    $('#app-body').animate({'top':'-25px'},1000).animate({'top':'15px'}, 250).animate({'top':'0px'},250);
    console.log('region_t: '+region_t);
    //setTimeout(function(){check_region();},1500);

    $('#add-getloc').click(function(e){
        getLocation(function(position){
            var geo = position.coords.latitude + ',' + position.coords.longitude;
            document.getElementById('coordinates').innerHTML = geo;
        }, null, true);
    });
    
    $("#mem-plus").click(function(e){              
        member+=1;

        var targ = document.getElementById('mem-body');
        var child_1 = document.createElement('div'); setAttributes(child_1, {'class':'mem-card', 'id':'mem-' + member});
        var child_2 = document.createElement('div'); setAttributes(child_2, {'class':'row-head', 'style':'height:2.5em;'});
        var child_27 = document.createElement('div'); setAttributes(child_27, {'class':'row-title'}); child_27.innerHTML = 'Member '+member;
        var child_3 = document.createElement('div'); setAttributes(child_3, {'class':'row-x btn', 'style':'font-size:1.15em; padding:0.2em; margin:0.3em;', 'onclick':'mem-remove('+member+')'}); child_3.innerHTML = 'X';

        var child_4 = document.createElement('div'); setAttributes(child_4, {'class':'row center'});
        var child_5 = document.createElement('input'); setAttributes(child_5, {'type':'text', 'placeholder':'First Name', 'id':'first-name', 'class':'member-'+member+' add-input add-member-'+member});
        child_4.appendChild(child_5);

        var child_6 = document.createElement('div'); setAttributes(child_6, {'class':'row center'});
        var child_7 = document.createElement('input'); setAttributes(child_7, {'type':'text', 'placeholder':'Last Name', 'id':'last-name', 'class':'member-'+member+' add-input add-member-'+member});
        child_6.appendChild(child_7);

        var child_10 = document.createElement('div'); setAttributes(child_10, {'class':'row center'});
        var child_11 = document.createElement('input'); setAttributes(child_11, {'type':'text', 'placeholder':'Other Name', 'id':'other-name', 'class':'member-'+member+' add-input add-member-'+member});
        child_10.appendChild(child_11);

        var child_12 = document.createElement('div'); setAttributes(child_12, {'class':'row center'});
        var child_13 = document.createElement('input'); setAttributes(child_13, {'type':'text', 'placeholder':'NIC Number', 'id':'nic', 'class':'member-'+member+' add-input add-member-'+member});
        child_12.appendChild(child_13);

        var child_14 = document.createElement('div'); setAttributes(child_14, {'class':'row center'});
        var child_26 = document.createElement('div'); setAttributes(child_26, {'style':'width:50%;font-size: 0.6em;padding-left:5%'}); child_26.innerHTML = 'Date Of Birth: ';
        var child_15= document.createElement('input'); setAttributes(child_15, {'type':'date', 'placeholder':'Date Of Birth', 'id':'dob', 'class':'member-'+member+' add-input add-member-'+member});
        child_14.appendChild(child_26);
        child_14.appendChild(child_15);

        var child_16 = document.createElement('div'); setAttributes(child_16, {'class':'row center'});
        var child_17= document.createElement('input'); setAttributes(child_17, {'type':'number', 'placeholder':'Age', 'id':'age', 'class':'member-'+member+' add-input add-member-'+member});
        child_16.appendChild(child_17);

        var child_18 = document.createElement('div'); setAttributes(child_18, {'class':'row center'});
        var child_19= document.createElement('input'); setAttributes(child_19, {'type':'text', 'placeholder':'Mobile Number', 'id':'mobile', 'class':'member-'+member+' add-input add-member-'+member});
        child_18.appendChild(child_19);

        var child_20 = document.createElement('div'); setAttributes(child_20, {'class':'row center'});
        var child_21= document.createElement('input'); setAttributes(child_21, {'type':'email', 'placeholder':'Email Address', 'id':'email', 'class':'member-'+member+' add-input add-member-'+member});
        child_20.appendChild(child_21);

        var child_22 = document.createElement('div'); setAttributes(child_22, {'class':'row center'});
        var child_23 = document.createElement('input'); setAttributes(child_23, {'type':'text', 'placeholder':'Occupation', 'id':'occupation', 'class':'member-'+member+' add-input add-member-'+member});
        child_22.appendChild(child_23);

        var child_24 = document.createElement('div'); setAttributes(child_24, {'class':'row center'});
        var child_25 = document.createElement('input'); setAttributes(child_25, {'type':'number', 'placeholder':'Monthly salary', 'id':'salary', 'class':'member-'+member+' add-input add-member-'+member});
        child_24.appendChild(child_25);
        
        child_1.appendChild(child_2);
        child_2.appendChild(child_3);
        child_2.appendChild(child_27);
        child_1.appendChild(child_4);
        child_1.appendChild(child_6);
        child_1.appendChild(child_10);
        child_1.appendChild(child_12);
        child_1.appendChild(child_14);
        child_1.appendChild(child_16);
        child_1.appendChild(child_18);
        child_1.appendChild(child_20);
        child_1.appendChild(child_22);
        child_1.appendChild(child_24);
        targ.appendChild(child_1);

        var leftPos = $('#mem-body').scrollLeft();
        $("#mem-body").animate({scrollLeft: leftPos + document.getElementById('mem-' + member).offsetLeft}, 800);

    });

    $('#adds').keyup(function(e){
        var el = document.getElementById('adds');
        var h = el.value.length;
        var dis_w = document.body.offsetWidth;
        var height = h/(dis_w/13) * 20;
        
        el.style.height = height + 'px';


    });

    $('#add-situation-textarea').keyup(function(e){
        var el = document.getElementById('add-situation-textarea');
        var h = el.value.length;
        var dis_w = document.body.offsetWidth;
        var height = h/(dis_w/13) * 13;
        
        el.style.height = height + 'px';


    });

    $('#add-pic').click(function(){
        getImage(function(imageURI){
            onSuccess(imageURI);
        }, function(msg){
            notif('Failed to add photo: ' + msg);
        });
    });

    $('#region').keyup(function(e){
        lead_region(this.value)
    });

    $(document).click(function(e){
        console.log(e.target.id);
        if(e.target.id != 'region'){
            hide_dropdown();
        };
        if(e.target.class == 'add-img'){
            var el = document.createElement('img'); setAttributes(el, {'class':'img-preview'})
        }else if(e.target.id == 'overlay-x'){
            hide_overlay();
        }
    });

    $('#add-issue-add-btn').click(function(e){
        add_send_issue();
    });

    $('#add-solution-add-btn').click(function(e){
        add_send_solution();
 
    });

    $('#add-solution-input').keyup(function(e){
        if(e.keyCode == 13){
             add_send_solution();
        }
    });

    $('#add-issue-input').keyup(function(e){
        if(e.keyCode == 13){
             add_send_issue();
        }
    });
}

function add_send_issue(){
    var el = document.getElementById('add-issue-input');
    var val = el.value;
    if(val != ""){
        add_issue_count += 1;
        document.getElementById('add-issues').innerHTML += "<div class = 'row center' id = 'add-issue-"+add_issue_count+"'><div class = 'add-issue'>"+val+"</div><div class = 'add-btn' onclick = \"add_issue_remove(\'add-issue-"+add_issue_count+"\')\"> - </div></div>";
        el.value ='';
        $('#add-issue-input').focus();
    }else{
        notif('No text in the field');
    }
}

function add_send_solution(){
    el = document.getElementById('add-solution-input');
    var val = el.value;
    if(val != ""){
        add_solution_count += 1;
        document.getElementById('add-solutions').innerHTML += "<div class = 'row center' id = 'add-solution-"+add_solution_count+"'><div class = 'add-solution'>"+val+"</div><div class = 'add-btn' onclick = \"add_issue_remove(\'add-solution-"+add_solution_count+"\')\"> - </div></div>";
        el.value ='';
        $('#add-solution-input').focus();
    }else{
        notif('No text in the field');
    }
}

function add_issue_remove(el){
    rm_el(document.getElementById(el));
}

function mem_remove(num){
    var el = document.getElementById('mem-'+num);
    el.parentNode.removeChild(el);
}

function align_dropdown(){
    try{
        var source = document.getElementById('region');
        var sink = document.getElementById('dropdown');
        var pos = getPos(source);
        var height = source.offsetHeight;
        var width = source.offsetWidth;

        sink.style.top = (height+pos.y).toString() + "px";
        sink.style.left = (pos.x).toString() + "px";
        sink.style.width = width.toString() + "px";
        disp_over('dropdown aligned');  
    }catch(err){}

}
function show_dropdown(){    
    document.getElementById('drop').innerHTML+="<div id='dropdown'></div>";
    disp_over('dropdown shown');
    align_dropdown();
}
function hide_dropdown(){
    try{
        var el = document.getElementById('dropdown');
        el.parentNode.removeChild(el);
    }catch(err){}

}
function display_dropdown_cont(data){
    hide_dropdown();
    show_dropdown();
    var el = document.getElementById('dropdown');
    el.innerHTMl="";
    for(var i =0; i < data.length; i++){
        el.innerHTML += "<div class = 'dropdown-cont btn' onclick=\"dropdown_select('"+data[i]+"')\">"+data[i]+"</div>";
    }
}
function dropdown_select(sel){
    disp_over('Selection detected from dropdown...');
    document.getElementById('region').value = sel;
    hide_dropdown();
}

function cond(el){
    if(el == 'cond'){
        document.getElementById('cond-value').value = document.getElementById('cond').value;
    }else if( el == 'cond-value'){
        document.getElementById('cond').value = document.getElementById('cond-value').value;
    }
    
}

function createDB(tx){
    tx.executeSql('DROP TABLE IF EXISTS pics');
    tx.executeSql( 'CREATE TABLE IF NOT EXISTS region(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, date TEXT NOT NULL)');
    //tx.executeSql( 'CREATE TABLE IF NOT EXISTS adcas(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, data TEXT NOT NULL, date TEXT NOT NULL)',[],nullHandler,errorHandler);
    tx.executeSql( 'CREATE TABLE IF NOT EXISTS pics(id INTEGER PRIMARY KEY AUTOINCREMENT, data TEXT)');
}
function getPos(el){

    for (var lx=0, ly=0;
         el != null;
         lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
    return {x: lx,y: ly};
}

/*------------------------------DATA-------------------------------------*/
function send(){
    var data = data_add();
    disp_over(data);
    online(function(){        
        document.getElementById('overlay').style.display='unset';
        document.getElementById('overlay-x').style.display='unset';
        disp_over('Uploading data...');
        $.ajax({
            type:'POST',
            dataType:'json',
            data:{data:data},
            url:'http://www.roh.nurmaan.free-staging-server.us/api/add',
            success:function(result){
                document.getElementById('overlay').style.display='none';
                document.getElementById('overlay-x').style.display='none';
                if(result.result){
                    disp_over('Successfully sent data to server. Uploading Images...');
                    save_pics(JSON.stringify(pic_id_assoc));
                }else{
                    notif('Could not complete operation right now. Data has been saved locally to upload later');
                    disp_over('A server error occurred. We will keep trying...');
                    //save_data(data);
                }
            },error:function(){
                notif('Could not complete operation right now. Data has been saved locally to upload later');
                disp_over('An error occurred. We Will keep trying...');
                //save_data(data);
            }
        })
    }, function(msg){
        disp_over(msg);
        disp_over('Could not send data due to a network error. We will keep trying...');
    });

}
function data_add(){
    var data = {'other':{}, 'member':{}, 'situation':{}};
    var other = document.getElementsByClassName('add-other');
    for(var i =0; i < other.length; i++){
        var id = other[i].id;
        var val = other[i].value
        data['other'][id] = val;
    }
    for(var i=0; i<member; i++){
        /*data['member-'+i]={};*/
        data['member'][i]={};
        console.log('add-member-'+(i+1));
        var el = document.getElementsByClassName('add-member-'+(i+1));
        console.log(el.length);
        for(var j =0; j < el.length; j++){
            var id = el[j].id;
            var val =el[j].value;
            data['member'][i][id] = val;
        }
    }
    var add_issue_el = document.getElementsByClassName('add-issue');
    data['situation']['issue'] = {};
    for(var i =0; i < add_issue_el.length; i++){
        data['situation']['issue'][i] = add_issue_el[i].innerHTML;
    }
    var add_solution_el = document.getElementsByClassName('add-solution');
    data['situation']['solution'] = {};
    for(var i =0; i < add_solution_el.length; i++){
        data['situation']['solution'][i] = {'solution':{},'state':{}};
        data['situation']['solution'][i]['solution'] = add_solution_el[i].innerHTML;
        data['situation']['solution'][i]['state'] = 'unchecked';
    }
    console.log(user_id);
    data['other']['picture'] = JSON.stringify(pic_id);
    data['u_id'] = {'user_id' : user_id};
    console.log(data);
    return JSON.stringify(data);

}
function save_data(data){
    disp_over('Saving data...');
    db.transaction(function(tx){
        tx.executeSql("INSERT INTO temp (data, url, success, server_error, connection_error) VALUES ('"+data+"', 'http://www.roh.nurmaan.free-staging-server.us/api/add','add_temp_success', 'add_temp_server_error', 'add_temp_connection_error')");
        tx.executeSql("SELECT * FROM temp;",[],function(tx,result){console.log(result)}, errorHandler);
    }, errorCB, save_pics(JSON.stringify(pic_id_assoc)));
}

function add_temp_success(data){
    disp_over('Successfully sent data to server. Uploading Images...');
    for(var i = 0; i < pic.length; i++){
        upload_img(pic[i], function(){}, function(){});
    }
}

function add_temp_server_error(data){
    disp_over('A server error occurred. We will keep trying...');
}

function add_temp_connection_error(data){
    disp_over('An error occurred. We Will keep trying...');
}
/*-----------------------------PICUTRES---------------------------------*/
function display_image(imageURI, ind){
    var el0 = document.createElement('div'); setAttributes(el0, {'class':'img-cont', 'id':'img-'+ind});
    var el1= document.createElement('img'); setAttributes(el1, {'src':imageURI, 'class':'add-img'});
    var el2= document.createElement('div'); setAttributes(el2, {'class':'row center'});
    el2.appendChild(el1);
    el0.appendChild(el2);

    var el3 = document.createElement('div'); setAttributes(el3, {'class':'row'});
    var el4 = document.createElement('div'); setAttributes(el4, {'class':'add-btn'}); el4.innerHTML = '-Remove';
    el3.appendChild(el4);
    el0.appendChild(el3);

    var targ = document.getElementById('pic-cont');
    targ.appendChild(el0);

    init_pln_photo();
}

function upload_img(imageURI, name, success, err){
    disp_over('uploading image...');
    var options = new FileUploadOptions();
    options.fileKey ="file";
    options.fileName = name;
    options.mimeType ="image/jpeg ";
    options.chunkedMode = false;

    var ft = new FileTransfer();
    ft.upload(imageURI, "http://www.roh.nurmaan.free-staging-server.us/api/upload_img", function(r) {
        success(r);
    },
    function(error) {
        //document.getElementById('picture_msg').innerHTML = "Upload failed: Code = "+error.code;
        err(error);
    }, options);
}  

function save_pics(data){
    if(pic.length > 0){
        disp_over('Saving pictures locally...');
        disp_over(data);
        //console.log(data);
        db.transaction(function(tx){
            tx.executeSql('INSERT INTO pics (data) VALUES (\''+data+'\')', [],nullHandler,errorHandler);
            tx.executeSql('SELECT * FROM pics;', [], function(tx, result){disp_over(JSON.stringify(result));console.log(result)}, errorHandler);
        }, errorCB, function(){
            disp_over('Pictures saved to database...');
            pic = [];
            pic_id = [];
            pic_id_assoc = [];
            pic_late_upload();
        });  
    }else{
        disp_over('No pictures to upload');
    }


}

function onSuccess(imageURI) {      
    if(pic.indexOf(imageURI) == -1){
        notif('Successfully picked image...');
        pic.push(imageURI);
        var num = generate(25);
        pic_id_assoc[imageURI] = num;
        pic_id.push(num);
        var ind = pic.indexOf(imageURI);
        disp_over(ind);
        display_image(imageURI, ind);

    
       /* upload_img(imageURI, function(r){
            notif('upload successsful...');
            document.getElementById('picture_msg').innerHTML = "Upload successful: "+r.bytesSent+" bytes uploaded.";
        });*/
    }else{
        notif('Image Already Exists!');
    }
}

function pic_late_upload(){
    disp_over('Checking images to upload...');
    db.transaction(function(tx){
        tx.executeSql('SELECT * FROM pics;',[],function(tx, result){
            disp_over(JSON.stringify(result));
            if(result.rows.length > 0 && result != null){
                disp_over('Found '+result.rows.length+' entry to upload...');
                for(var i = 0; i < result.rows.length; i++){
                    disp_over('Checking internet connection...');
                    online(function(){
                        disp_over('Connection to server established');
                        console.log(result);
                        var dat = result.rows.item(0)['data'];
                        disp_over(dat);
                        var data = JSON.parse(dat);
                        var num = Objecy.keys.size(data);
                        console.log('Object size:  ' + num);
                        var success = 0;
                        for (var key in data) {
                            if (data.hasOwnProperty(key)) {
                                console.log(key + " -> " + data[key]);

                                upload_img(key, data[key], function(r){
                                    disp_over('Successfully uploaded one image...');
                                    success ++;
                                }, function(error){
                                    disp_over('Failed to upload image');
                                    console.log(error);
                                });
                            }
                            if(success == num){
                                db.transaction(function(tx){
                                    tx.executeSql('DELETE FROM pics WEHRE id = "' + result.rows.item(0)['id']+'"',[],nullHandler,errorHandler);
                                },errorCB, successCB);
                            }
                        }
                    }, function(){
                        disp_over('You are Offline. Trying again in 30s...');
                        temp_pic_t = 30000;
                        setTimeout(function(){pic_late_upload();}, temp_pic_t);
                    });
                }
  
            }else{
                disp_over('No images to upload');
                temp_pic_t = 2000;
            }
        }, errorHandler)
    }, errorCB, successCB);
}
function show_db_pics(){
    disp_over("Attempting to display database | pics...");
    db.transaction(function(transaction) {
       transaction.executeSql('SELECT * FROM pics;', [],
         function(transaction, result) {
          if (result != null && result.rows != null) {
            for (var i = 0; i < result.rows.length; i++) {
              var row = result.rows.item(i);
              disp_over('<br>' + row.data + ' | ' +
    row.id);
            }
          }
         },errorHandler);
     },errorHandler,nullHandler);
}

/*------------------------------REGION-----------------------------------*/
function store_region(data, count){
    if(count<data.length){
        db.transaction(function(tx){
            tx.executeSql("SELECT * FROM region WHERE name LIKE '%"+data[count]+"%';", [], function(tx, result){
                if(result !=null && result.rows.length != 0){
                    disp_over('Region name '+data[count]+' already exists...');
                }else{
                    tx.executeSql('INSERT INTO region (name, date) VALUES ("'+data[count]+'", datetime(\'now\'))', [], nullHandler, errorHandler);
                    
                }
            });
        },errorCB, function(){
            count ++;
            store_region(data,count);
        });
    }else{
        disp_over("Database saving done");
        show_db_region();
    }    
}

function lead_region(key){
    disp_over('Retrieving from local database...');
    db.transaction(function(tx){
        tx.executeSql("SELECT name FROM region WHERE name LIKE '%"+key+"%'", [], function(tx, result){
            if(result !=null && result.rows != null){
                var data = [];
                for(var i =0; i< result.rows.length;i++){
                    data.push(result.rows[i]['name']);
                }
                disp_over(data);
                display_dropdown_cont(data);
            }
        });
    }, errorCB, successCB);
}

function show_db_region(){
    disp_over("Attempting to display database data...")
    db.transaction(function(tx) {
        tx.executeSql('SELECT * FROM region;', [],
            function(tx, result) {
                if (result != null && result.rows != null) {
                    for (var i = 0; i < result.rows.length; i++) {
                        var row = result.rows.item(i);
                        $('#overlay').append('<br>' + row['id'] + '. ' +
                        row['name']+ ' ' + row['date']);
                    }
                }
            },function(error){disp_over('error 2: '+error.message)});
 },errorCB,function(){
    disp_over('Display succeeded');

 });
}

function check_region(count){
    var region = setTimeout(function(){
        disp_over('checking connectivity to load regions from internet...');
        online(function(){
            window.clearTimeout(region);
            disp_over('connection established to the server...');
            fetch_region(count);
        }, function(){
            count++;
            db.transaction(function(tx){
                tx.executeSql("SELECT * FROM region", [], function(tx, result){
                    disp_over(result);
                    if(result != null && result.rows.length != 0){
                        region_t = 300000;
                    }else if(region_t < 10000){
                        region_t = region_t + (count*6);
                    }else{
                        region_t = 10000;
                    }
                });
            }, errorCB, successCB); 
            
            disp_over('Failed to establish connection to the internet. Trying again in '+region_t/1000+'s');
            check_region(count); 
        });        
  
        

    }, region_t);
}

function fetch_region(){
    disp_over('Connecting to server to fetch regions...');
    $.ajax({
        type:'POST',
        url:'http://www.roh.nurmaan.free-staging-server.us/api/fetch_region',
        dataType:'json',
        timeout:2000,
        success:function(data){
            disp_over("Regions successfully loaded from server...");
            region_t = 500;
            disp_over('Saving regions to local database...');
            store_region(data,0);

        },
        error:function(data){
            disp_over('An error occured while trying to connect to the server...');
            check_region(0);
        }
    });
}