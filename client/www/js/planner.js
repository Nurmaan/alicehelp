func['planner'] = function(variable){
    console.log('variable: '+variable);
	if(typeof variable != 'undefined' && variable.length<4){
		fetch_planner(variable);
	}else{
		fetch_planner('all');
	}
	init_auto_height();
	show_cv();

	$('#pln-openmap').click(function(e){
		getLocation(function(position){
			show_map(position.coords.latitude, position.coords.longitude, null, "Current Position");
		}, function(error){
			show_map(0, 0, null, "");
		}, true);
	});

	$('.pln-openmap').click(function(e){
		var id = this.id.substr(this.id.indexOf('-') + 1);
		console.log(id);

		show_map("","", document.getElementById('coord-'+id).innerHTML, document.getElementById('pln-title-'+id).innerHTML)
	});

	$('#pln-all-btn').click(function(){
		fetch_planner('all');
	});

	$('#pln-my-btn').click(function(){
		fetch_planner('my');
	});

	$('#pln-completed-btn').click(function(){
		fetch_planner('completed');
	});

	if(variable == 'add'){
		show_pln_add();

	}
}

function fetch_planner(type){
	console.log('type: '+type )
	online(function(){
		$.ajax({
	        type:'POST',
	        url:'http://www.roh.nurmaan.free-staging-server.us/api/fetch_schedule_'+type,
	        dataType:'json',
	        data:{user_id:user_id},
	        success:function(data){
                if(data.result){
                	console.log(data.data);
                	if(type == 'all'){ 
                	    document.getElementById('pln-all-btn').className = 'upper-bar-comp btn active-btn'; 
                	    document.getElementById('pln-my-btn').className = 'upper-bar-comp btn'; 
                	    document.getElementById('pln-completed-btn').className = 'upper-bar-comp btn'; 
                	}else if(type == 'my'){
                	    document.getElementById('pln-my-btn').className = 'upper-bar-comp btn active-btn'; 
                	    document.getElementById('pln-all-btn').className = 'upper-bar-comp btn';                 		
                	    document.getElementById('pln-completed-btn').className = 'upper-bar-comp btn';                 		
                	}else{
                	    document.getElementById('pln-completed-btn').className = 'upper-bar-comp btn active-btn'; 
                	    document.getElementById('pln-all-btn').className = 'upper-bar-comp btn';                 		
                	    document.getElementById('pln-my-btn').className = 'upper-bar-comp btn';  
                	}
                	display_planner[type](data.data);
                }else{
                	notif('Failed to load data. Please try again');
                }

	        },
	        error:function(data){
	            notif('Failed to load data. Please try again');
	        }
	    });
	}, function(){
		notif('Sorry, could not complete the process because you are offline');
	});	
}

display_planner['all'] = function(data){
    var targ = document.getElementById('planner-body');
    targ.innerHTML = '';
    var cont ='';
    for(var i=0; i<data.length; i++){
        var validated_cont ='';
        var validated = data[i]['validated'];
    	if(validated != null){
    		for(var j =0; j<validated.length; j++){
    			validated_cont += "<div class = 'cd-row-label add-btn' style='width:unset;margin-bottom:0.4em;'>"+validated[j]['first_name']+" "+validated[j]['last_name']+"</div>"
    		}
    		console.log(validated_cont);
    	}

    	cont = 
    	"<div class='pln-elem row center'>"+
    	"<div class='pln-date'>"+data[i]['dt']+"</div>"+

    	"<div class='pln-cont card cd-half' id ='pln-cont-"+i+"'>"+
    		"<div class='row center'>"+
    		    "<div class='pln-title' id='pln-title-"+i+"'>"+data[i]['title']+"</div>"+
    		    "<div class='add-btn pln-ext-btn' id='plnextbtn-"+i+"'>+</div>" + 	
    		"</div>"+

    		"<div id = 'pln-body-"+i+"' style='display:none;'>"+

    		    "<div class = 'row center'>"+
    		        "<div style='display:inline-block;font-size:0.6em'>HOST </div>"+
    		        "<div class='pln-creator' id='u_id"+data[i]['user_id']+"'>"+data[i]['creator']+"</div>"+
    		    "</div>"+

                "<div class='pln-box'>"+
    				"<div class='pln-box-title'>Description</div>"+
    				"<div class='row center'>"+
    					"<div class = 'cd-row-label' style='width:100%;'>"+data[i]['description']+"</div>"+
    				"</div>" +
    		    "</div>"+

    			"<div class='pln-box'>"+
    				"<div class='pln-box-title'>Location</div>"+
    				"<div class='row center'>"+
    					"<div class = 'cd-row-label' style='width:100%;' id='coord-"+i+"'>"+data[i]['coord']+"</div>"+
    				"</div>" +  			
    			    "<div class='row center'><div class='add-btn pln-openmap' id = 'plnopenmap-"+i+"'>View on map</div></div>"+
    		    "</div>"+

    		    "<div class='pln-box'>"+
    				"<div class='pln-box-title'>Time</div>"+
    				"<div class='row center'>"+
    					"<div class = 'cd-row-label' style='width:30%;'>Start Time</div>"+
    					"<div class = 'cd-row-label' style='width:65%;'>"+data[i]['start_time']+"</div>"+
    				"</div>"+	
    				"<div class='row center'>"+
    					"<div class = 'cd-row-label' style='width:30%;'>End Time</div>"+
    					"<div class = 'cd-row-label' style='width:65%;'>"+data[i]['end_time']+"</div>"+
    				"</div>"+		
    			    
    		    "</div>"+

    		    "<div class='pln-box'>"+
    				"<div class='pln-box-title'>Type</div>"	+		
    			    "<div class = 'cd-row-label' style='width:100%;'>"+data[i]['state']+"</div>"+
    		    "</div>"+

    		    "<div class='pln-box'>"+
    				"<div class='pln-box-title'>Going</div>"+
                     validated_cont+
    		    "</div>"+
    		"</div>"+    	
    	"</div>";

    	targ.innerHTML += cont;
        if(data[i]['user']){
        	document.getElementById('pln-body-'+i).innerHTML += "<div class = 'row center'><div class ='add-btn' onclick=\"pln_not_going('"+data[i]['u_id']+"')\">I can't come</div></div>";
        }else{
        	document.getElementById('pln-body-'+i).innerHTML += "<div class = 'row center'><div class ='add-btn' onclick=\"pln_going('"+data[i]['u_id']+"')\">I will be there</div></div>";


        }
    	
    }
    var pln_add = 
   "<div id = 'pln-add'>"+
        "<div id='pln-add-x'>X</div>"+
        "<div id='pln-add-cont'>"+
	    	"<div class ='row center'>"+
	    		"<div class='card cd-full'>"+
	    			"<div class='cd-title'>Create Event</div>"+

	                "<div class='pln-box'>"+
	    				"<div class='pln-box-title'>General Info</div>"+
	    				"<div class='row center'><input name='title' type='text' id='pln-title'class='add-input pln-sub' placeholder='Title' maxlength='60'></div>"+
	    				"<div class='row center'><textarea name='description' class='add-input pln-sub auto_height' id='pln-description' placeholder='Description' maxlength='300'></textarea></div>"+
	    			"</div>"+

	    			"<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Location</div>"+
	    				"<div class='row center'>"+
	    					"<div class='add-btn' id='pln-getloc'>Get</div>"+
	    					"<input type='text' name='coord' id='lbl-pln-getloc' class='add-input pln-sub' placeholder='No current position'></input>"+
	    				"</div>"+
	    			    "<div class='row center'><div class='add-btn' id='pln-openmap'>Go to Maps</div></div>"+
	    			"</div>"+

	    			"<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Date and Time</div>"+
	    				"<div class='row center'>"+
	    					"<div class='cd-row-label'>Date</div>"+
	    					"<input type='date' id='pln-input-date' class='add-input  pln-sub' name='date'></input>"+
	    				"</div>"+

	    				"<div class='row center'>"+
	    					"<div class='cd-row-label'>Start Time</div>"+
	    					"<input type='time' id='pln-input-startTime' class='add-input  pln-sub' name='start_time'></input>"+
	    				"</div>"+

	    				"<div class='row center'>"+
	    					"<div class='cd-row-label'>End Time</div>"+
	    					"<input type='time' id='pln-input-endTime' class='add-input  pln-sub' name='end_time'></input>"+
	    				"</div>"+
	    			"</div>"+

	    			"<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Audience</div>"+
	    			    "<div class='row center'>"+
	    			    	"<input type='radio' name='pln-aud' id = 'pln-aud-pub' class='pln-sub'>"+
	    			    	"<label class='check-label'>Public</label>"+
	    			    "</div>"+
	    			    "<div class = 'row center'>    			    	"+
	    			    	"<input type='radio' name='pln-aud' id = 'pln-aud-pri' class='pln-sub'>"+
	    			    	"<label class='check-label'>Private</label>"+
	    			    "</div>"+
	    			"</div>"+

	    			"<div class='pln-box'  style='display:none'>"+
	    				"<div class='pln-box-title'>Case ID</div>"+
	    			    "<div class='row center'>"+
	    			    	"<input type='text' name='case_id' id = 'pln-case_id' class='pln-sub add-input' readonly>"+
	    			    "</div>"+
	    			"</div>"+
	    		"</div>"+
	    	"</div>"+

		    "<div class='row center'>"+
				"<div class='add-btn' onclick='pln_submit()' id='pln_submit_btn'>Submit</div>"+
			"</div>"+
		"</div>"+
    "</div>";
    targ.innerHTML+=pln_add;
    init_pln_listener();
}

display_planner['my'] = function(data){
	var targ = document.getElementById('planner-body');
    targ.innerHTML = '';
    var cont ='';
    for(var i=0; i<data.length; i++){
        var validated_cont ='';
        var validated = data[i]['validated'];
    	if(validated != null){
    		for(var j =0; j<validated.length; j++){
    			validated_cont += "<div class = 'cd-row-label add-btn' style='width:unset;margin-bottom:0.4em;'>"+validated[j]['first_name']+" "+validated[j]['last_name']+"</div>"
    		}
    		console.log(validated_cont);
    	}

    	cont = 
    	"<div class='pln-elem row center'>"+
    	"<div class='pln-date'>"+data[i]['dt']+"</div>"+

    	"<div class='pln-cont card cd-half' id ='pln-cont-"+i+"'>"+
    		"<div class='row center'>"+
    		    "<div class='pln-title' id='pln-title-"+i+"'>"+data[i]['title']+"</div>"+
    		    "<div class='add-btn pln-ext-btn' id='plnextbtn-"+i+"'>+</div>" + 	
    		"</div>"+

    		"<div id = 'pln-body-"+i+"' style='display:none;'>"+

    		    "<div class = 'row center'>"+
    		        "<div style='display:inline-block;font-size:0.6em'>HOST </div>"+
    		        "<div class='pln-creator' id='u_id"+data[i]['user_id']+"'>"+data[i]['creator']+"</div>"+
    		    "</div>"+

                "<div class='pln-box'>"+
    				"<div class='pln-box-title'>Description</div>"+
    				"<div class='row center'>"+
    					"<div class = 'cd-row-label' style='width:100%;'>"+data[i]['description']+"</div>"+
    				"</div>" +
    		    "</div>"+

    			"<div class='pln-box'>"+
    				"<div class='pln-box-title'>Location</div>"+
    				"<div class='row center'>"+
    					"<div class = 'cd-row-label' style='width:100%;' id='coord-"+i+"'>"+data[i]['coord']+"</div>"+
    				"</div>" +  			
    			    "<div class='row center'><div class='add-btn pln-openmap' id = 'plnopenmap-"+i+"'>View on map</div></div>"+
    		    "</div>"+

    		    "<div class='pln-box'>"+
    				"<div class='pln-box-title'>Time</div>"+
    				"<div class='row center'>"+
    					"<div class = 'cd-row-label' style='width:30%;'>Start Time</div>"+
    					"<div class = 'cd-row-label' style='width:65%;'>"+data[i]['start_time']+"</div>"+
    				"</div>"+	
    				"<div class='row center'>"+
    					"<div class = 'cd-row-label' style='width:30%;'>End Time</div>"+
    					"<div class = 'cd-row-label' style='width:65%;'>"+data[i]['end_time']+"</div>"+
    				"</div>"+		
    			    
    		    "</div>"+

    		    "<div class='pln-box'>"+
    				"<div class='pln-box-title'>Type</div>"	+		
    			    "<div class = 'cd-row-label' style='width:100%;'>"+data[i]['state']+"</div>"+
    		    "</div>"+

    		    "<div class='pln-box'>"+
    				"<div class='pln-box-title'>Going</div>"+
                     validated_cont+
    		    "</div>"+
    		    "<div class = 'row center'><div class ='add-btn' onclick=\"pln_cancel_event('"+data[i]['u_id']+"')\">Cancel Event</div></div>"
    		"</div>"+    	
    	"</div>";
        
    	targ.innerHTML += cont;
        if(data[i]['user']){
        	document.getElementById('pln-body-'+i).innerHTML += "<div class = 'row center'><div class ='add-btn' onclick=\"pln_not_going('"+data[i]['u_id']+"')\">I can't come</div></div>";
        }else{
        	document.getElementById('pln-body-'+i).innerHTML += "<div class = 'row center'><div class ='add-btn' onclick=\"pln_going('"+data[i]['u_id']+"')\">I will be there</div></div>";


        }
    	
    }
    var pln_add = 
   "<div id = 'pln-add'>"+
        "<div id='pln-add-x'>X</div>"+
        "<div id='pln-add-cont'>"+
	    	"<div class ='row center'>"+
	    		"<div class='card cd-full'>"+
	    			"<div class='cd-title'>Create Event</div>"+

	                "<div class='pln-box'>"+
	    				"<div class='pln-box-title'>General Info</div>"+
	    				"<div class='row center'><input name='title' type='text' id='pln-title'class='add-input pln-sub' placeholder='Title' maxlength='60'></div>"+
	    				"<div class='row center'><textarea name='description' class='add-input pln-sub auto_height' id='pln-description' placeholder='Description' maxlength='300'></textarea></div>"+
	    			"</div>"+

	    			"<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Location</div>"+
	    				"<div class='row center'>"+
	    					"<div class='add-btn' id='pln-getloc'>Get</div>"+
	    					"<input type='text' name='coord' id='lbl-pln-getloc' class='add-input pln-sub' placeholder='No current position'></input>"+
	    				"</div>"+
	    			    "<div class='row center'><div class='add-btn' id='pln-openmap'>Go to Maps</div></div>"+
	    			"</div>"+

	    			"<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Date and Time</div>"+
	    				"<div class='row center'>"+
	    					"<div class='cd-row-label'>Date</div>"+
	    					"<input type='date' id='pln-input-date' class='add-input  pln-sub' name='date'></input>"+
	    				"</div>"+

	    				"<div class='row center'>"+
	    					"<div class='cd-row-label'>Start Time</div>"+
	    					"<input type='time' id='pln-input-startTime' class='add-input  pln-sub' name='start_time'></input>"+
	    				"</div>"+

	    				"<div class='row center'>"+
	    					"<div class='cd-row-label'>End Time</div>"+
	    					"<input type='time' id='pln-input-endTime' class='add-input  pln-sub' name='end_time'></input>"+
	    				"</div>"+
	    			"</div>"+

	    			"<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Audience</div>"+
	    			    "<div class='row center'>"+
	    			    	"<input type='radio' name='pln-aud' id = 'pln-aud-pub' class='pln-sub'>"+
	    			    	"<label class='check-label'>Public</label>"+
	    			    "</div>"+
	    			    "<div class = 'row center'>    			    	"+
	    			    	"<input type='radio' name='pln-aud' id = 'pln-aud-pri' class='pln-sub'>"+
	    			    	"<label class='check-label'>Private</label>"+
	    			    "</div>"+
	    			"</div>"+

	    			"<div class='pln-box'  style='display:none'>"+
	    				"<div class='pln-box-title'>Case ID</div>"+
	    			    "<div class='row center'>"+
	    			    	"<input type='text' name='case_id' id = 'pln-case_id' class='pln-sub add-input' readonly>"+
	    			    "</div>"+
	    			"</div>"+
	    		"</div>"+
	    	"</div>"+

		    "<div class='row center'>"+
				"<div class='add-btn' onclick='pln_submit()' id='pln_submit_btn'>Submit</div>"+

			"</div>"+
		"</div>"+
    "</div>";
    targ.innerHTML+=pln_add;
    init_pln_listener();
}

display_planner['completed'] = function(data){
	console.log(data);
	var targ = document.getElementById('planner-body');
    targ.innerHTML = '';
    var cont ='';
    for(var i=0; i<data.length; i++){
        var validated_cont ='';
        var validated = data[i]['validated'];
    	if(validated != null){
    		for(var j =0; j<validated.length; j++){
    			validated_cont += "<div class = 'cd-row-label add-btn' style='width:unset;margin-bottom:0.4em;'>"+validated[j]['first_name']+" "+validated[j]['last_name']+"</div>"
    		}
    		console.log(validated_cont);
    	}

    	


    	cont = 
    	"<div class='pln-elem row center'>"+
	    	"<div class='pln-date'>"+data[i]['dt']+"</div>"+

	    	"<div class='pln-cont card cd-half' id ='pln-cont-"+i+"'>"+
	    		"<div class='row center'>"+
	    		    "<div class='pln-title' id='pln-title-"+i+"'>"+data[i]['title']+"</div>"+
	    		    "<div class='add-btn pln-ext-btn' id='plnextbtn-"+i+"'>+</div>" + 	
	    		"</div>"+

	    		"<div id = 'pln-body-"+i+"' style='display:none;'>"+

	    		    "<div class = 'row center'>"+
	    		        "<div style='display:inline-block;font-size:0.6em'>HOST </div>"+
	    		        "<div class='pln-creator' id='u_id"+data[i]['user_id']+"'>"+data[i]['creator']+"</div>"+
	    		    "</div>"+

	                "<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Description</div>"+
	    				"<div class='row center'>"+
	    					"<div class = 'cd-row-label' style='width:100%;'>"+data[i]['description']+"</div>"+
	    				"</div>" +
	    		    "</div>"+

	    			"<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Location</div>"+
	    				"<div class='row center'>"+
	    					"<div class = 'cd-row-label' style='width:100%;' id='coord-"+i+"'>"+data[i]['coord']+"</div>"+
	    				"</div>" +  			
	    			    "<div class='row center'><div class='add-btn pln-openmap' id = 'plnopenmap-"+i+"'>View on map</div></div>"+
	    		    "</div>"+

	    		    "<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Time</div>"+
	    				"<div class='row center'>"+
	    					"<div class = 'cd-row-label' style='width:30%;'>Start Time</div>"+
	    					"<div class = 'cd-row-label' style='width:65%;'>"+data[i]['start_time']+"</div>"+
	    				"</div>"+	
	    				"<div class='row center'>"+
	    					"<div class = 'cd-row-label' style='width:30%;'>End Time</div>"+
	    					"<div class = 'cd-row-label' style='width:65%;'>"+data[i]['end_time']+"</div>"+
	    				"</div>"+		
	    			    
	    		    "</div>"+

	    		    "<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Type</div>"	+		
	    			    "<div class = 'cd-row-label' style='width:100%;'>"+data[i]['state']+"</div>"+
	    		    "</div>"+

	    		    "<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Going</div>"+
	                     validated_cont+
	    		    "</div>"+

	    		    "<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Pictures</div>"+
	    				"<div id='pln-picbox-"+i+"' class='pic-cont'></div>"+
	    				"<div class = 'row center'><div class ='add-btn' onclick=\"pln_photo('"+data[i]['u_id']+"', '"+i+"')\">+Photos</div></div>"+
	    		    "</div>"+    		    
	    		"</div>"+    	
	    	"</div>"+
    	"</div>";
        
    	targ.innerHTML += cont;
        
        var photo = JSON.parse(data[i]["photo_id"]);
        console.log(data[i]);
        console.log(photo);
        if(photo != null){
        	for(var j =0; j< Object.keys(photo).length; j++){
	            if(photo[i] != null){
	            	var el0 = document.createElement('div'); setAttributes(el0, {'class':'img-cont', 'id':'img-'+j});
				    var el1= document.createElement('img'); setAttributes(el1, {'src':'http://www.roh.nurmaan.free-staging-server.us/Users/Pictures/'+photo[j]['id'], 'class':'add-img'});
				    var el2= document.createElement('div'); setAttributes(el2, {'class':'row center'});
				    el2.appendChild(el1);
				    el0.appendChild(el2);

				    var el3 = document.createElement('div'); setAttributes(el3, {'class':'row'});
				    var el4 = document.createElement('div'); setAttributes(el4, {'class':'add-btn', 'onclick':'pln_remove_pic("'+data[i]['u_id']+'","'+j+'","'+photo[j]['id']+'")'}); el4.innerHTML = '-Remove';
				    el3.appendChild(el4);
				    el0.appendChild(el3);

				    var targt = document.getElementById('pln-picbox-' +i);
				    targt.appendChild(el0);
	            }

	    	}

        }
    	

    	
    }
    var pln_add = 
   "<div id = 'pln-add'>"+
        "<div id='pln-add-x'>X</div>"+
        "<div id='pln-add-cont'>"+
	    	"<div class ='row center'>"+
	    		"<div class='card cd-full'>"+
	    			"<div class='cd-title'>Create Event</div>"+

	                "<div class='pln-box'>"+
	    				"<div class='pln-box-title'>General Info</div>"+
	    				"<div class='row center'><input name='title' type='text' id='pln-title'class='add-input pln-sub' placeholder='Title' maxlength='60'></div>"+
	    				"<div class='row center'><textarea name='description' class='add-input pln-sub auto_height' id='pln-description' placeholder='Description' maxlength='300'></textarea></div>"+
	    			"</div>"+

	    			"<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Location</div>"+
	    				"<div class='row center'>"+
	    					"<div class='add-btn' id='pln-getloc'>Get</div>"+
	    					"<input type='text' name='coord' id='lbl-pln-getloc' class='add-input pln-sub' placeholder='No current position'></input>"+
	    				"</div>"+
	    			    "<div class='row center'><div class='add-btn' id='pln-openmap'>Go to Maps</div></div>"+
	    			"</div>"+

	    			"<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Date and Time</div>"+
	    				"<div class='row center'>"+
	    					"<div class='cd-row-label'>Date</div>"+
	    					"<input type='date' id='pln-input-date' class='add-input  pln-sub' name='date'></input>"+
	    				"</div>"+

	    				"<div class='row center'>"+
	    					"<div class='cd-row-label'>Start Time</div>"+
	    					"<input type='time' id='pln-input-startTime' class='add-input  pln-sub' name='start_time'></input>"+
	    				"</div>"+

	    				"<div class='row center'>"+
	    					"<div class='cd-row-label'>End Time</div>"+
	    					"<input type='time' id='pln-input-endTime' class='add-input  pln-sub' name='end_time'></input>"+
	    				"</div>"+
	    			"</div>"+

	    			"<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Audience</div>"+
	    			    "<div class='row center'>"+
	    			    	"<input type='radio' name='pln-aud' id = 'pln-aud-pub' class='pln-sub'>"+
	    			    	"<label class='check-label'>Public</label>"+
	    			    "</div>"+
	    			    "<div class = 'row center'>    			    	"+
	    			    	"<input type='radio' name='pln-aud' id = 'pln-aud-pri' class='pln-sub'>"+
	    			    	"<label class='check-label'>Private</label>"+
	    			    "</div>"+
	    			"</div>"+

	    			"<div class='pln-box'  style='display:none'>"+
	    				"<div class='pln-box-title'>Case ID</div>"+
	    			    "<div class='row center'>"+
	    			    	"<input type='text' name='case_id' id = 'pln-case_id' class='pln-sub add-input' readonly>"+
	    			    "</div>"+
	    			"</div>"+
	    		"</div>"+
	    	"</div>"+

		    "<div class='row center'>"+
				"<div class='add-btn' onclick='pln_submit()' id='pln_submit_btn'>Submit</div>"+

			"</div>"+
		"</div>"+
    "</div>";
    targ.innerHTML+=pln_add;
    init_pln_listener();
    init_pln_photo();
}


function large_img(src){
	console.log('large-image');
    var targ = document.body;
    var child1 = document.createElement('div'); setAttributes(child1, {'class':'cover', 'id':'large-image'});
    var child2 = document.createElement('div'); setAttributes(child2, {'class':'x btn', 'id':'large-image-x'}); child2.innerHTML = 'X';

    var child3 = document.createElement('img'); setAttributes(child3, {'class':'large-image','src':src});
     
    child1.appendChild(child2);
    child1.appendChild(child3);
    targ.appendChild(child1);

    $(".cover#large-image").css({'display':'unset'});

    $(".cover#large-image").animate({'opacity':'1'}, 1000);

    $("#large-image-x").click(function(){
    	rm_el(document.getElementById('large-image'));
    });

}

function pln_photo(schedule_id, id){
    getImage(function(imageURI){

        
        var name = generate(25);
        online(function(){
			$.ajax({
		        type:'POST',
		        url:'http://www.roh.nurmaan.free-staging-server.us/api/schedule_photo',
		        dataType:'json',
		        data:{user_id:user_id, name:name, schedule_id:schedule_id},
		        success:function(data){
	                if(data.result){
	                	console.log(data.data);
	                  	upload_img(imageURI, name, function(){
				            disp_over('Image successfully added to database...');
				            disp_over('http://www.roh.nurmaan.free-staging-server.us/users/pictures/'+data.data);
					    	var el0 = document.createElement('div'); setAttributes(el0, {'class':'img-cont img-'+id});
						    var el1= document.createElement('img'); setAttributes(el1, {'src':'http://www.roh.nurmaan.free-staging-server.us/Users/Pictures/'+data.data, 'class':'add-img'});
						    var el2= document.createElement('div'); setAttributes(el2, {'class':'row center'});
						    el2.appendChild(el1);
						    el0.appendChild(el2);

						    var el3 = document.createElement('div'); setAttributes(el3, {'class':'row'});
						    var el4 = document.createElement('div'); setAttributes(el4, {'class':'add-btn', 'onclick':'pln_remove_pic("'+schedule_id+'","'+id+'","'+data.data+'")'}); el4.innerHTML = '-Remove';
						    el3.appendChild(el4);
						    el0.appendChild(el3);

						    var targ = document.getElementById('pln-picbox-' +id);
						    targ.appendChild(el0);

						    init_pln_photo();

					    }, function(){
					    	notif('Failed to add photo to database. Please try again');
					    });
	                	
	                }else{
	                	notif('Failed to upload data. Please try again');
	                }

		        },
		        error:function(data){
		            notif('Failed tohkzgfsdk load data. Please try again');
		        }
		    });
		    }, function(){
			notif('Sorry, could not complete the process because you are offline');
		});	
    }, nullHandler);
}

function init_pln_photo(){
	$('.add-img').unbind().click(function(e){
		large_img(this.src);
		console.log('clicked');
	});
}

function pln_remove_pic(schedule_id, id, name){
    online(function(){
		$.ajax({
	        type:'POST',
	        url:'http://www.roh.nurmaan.free-staging-server.us/api/schedule_photo_remove',
	        dataType:'json',
	        data:{user_id:user_id, schedule_id:schedule_id, name:name},
	        success:function(data){
                if(data.result){
                	console.log(data.data);
                	notif('Successfully removed photo');
                	rm_el(document.getElementById('pln-picbox-'+id));
                  	
                }else{
                	console.log(data.code);
                	notif('Failed to upload data. Please try again');
                }

	        },
	        error:function(data){
	            notif('Failed tohkzgfsdk load data. Please try again');
	        }
	    });
	    }, function(){
		notif('Sorry, could not complete the process because you are offline');
	});		
}

function get_pln_add(){
    var pln_add = 
    "<div id = 'pln-add'>"+
        "<div id='pln-add-x'>X</div>"+
        "<div id='pln-add-cont'>"+
	    	"<div class ='row center'>"+
	    		"<div class='card cd-full'>"+
	    			"<div class='cd-title'>Create Event</div>"+

	                "<div class='pln-box'>"+
	    				"<div class='pln-box-title'>General Info</div>"+
	    				"<div class='row center'><input name='title' type='text' id='pln-title'class='add-input pln-sub' placeholder='Title' maxlength='60'></div>"+
	    				"<div class='row center'><textarea name='description' class='add-input pln-sub auto_height' id='pln-description' placeholder='Description' maxlength='300'></textarea></div>"+
	    			"</div>"+

	    			"<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Location</div>"+
	    				"<div class='row center'>"+
	    					"<div class='add-btn' id='pln-getloc'>Get</div>"+
	    					"<input type='text' name='coord' id='lbl-pln-getloc' class='add-input pln-sub' placeholder='No current position'></input>"+
	    				"</div>"+
	    			    "<div class='row center'><div class='add-btn' id='pln-openmap'>Go to Maps</div></div>"+
	    			"</div>"+

	    			"<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Date and Time</div>"+
	    				"<div class='row center'>"+
	    					"<div class='cd-row-label'>Date</div>"+
	    					"<input type='date' id='pln-input-date' class='add-input  pln-sub' name='date'></input>"+
	    				"</div>"+

	    				"<div class='row center'>"+
	    					"<div class='cd-row-label'>Start Time</div>"+
	    					"<input type='time' id='pln-input-startTime' class='add-input  pln-sub' name='start_time'></input>"+
	    				"</div>"+

	    				"<div class='row center'>"+
	    					"<div class='cd-row-label'>End Time</div>"+
	    					"<input type='time' id='pln-input-endTime' class='add-input  pln-sub' name='end_time'></input>"+
	    				"</div>"+
	    			"</div>"+

	    			"<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Audience</div>"+
	    			    "<div class='row center'>"+
	    			    	"<input type='radio' name='pln-aud' id = 'pln-aud-pub' class='pln-sub'>"+
	    			    	"<label class='check-label'>Public</label>"+
	    			    "</div>"+
	    			    "<div class = 'row center'>    			    	"+
	    			    	"<input type='radio' name='pln-aud' id = 'pln-aud-pri' class='pln-sub'>"+
	    			    	"<label class='check-label'>Private</label>"+
	    			    "</div>"+
	    			"</div>"+

	    			"<div class='pln-box'>"+
	    				"<div class='pln-box-title'>Case ID</div>"+
	    			    "<div class='row center'>"+
	    			    	"<input type='text' name='case_id' id = 'pln-case_id' class='pln-sub add-input' readonly>"+
	    			    "</div>"+
	    			"</div>"+
	    		"</div>"+
	    	"</div>"+

		    "<div class='row center'>"+
				"<div class='add-btn' onclick='pln_submit()' id='pln_submit_btn'>Submit</div>"+
			"</div>"+
		"</div>"+
    "</div>";

    return pln_add;

}

function pln_going(schedule_id){
	 online(function(){
		$.ajax({
	        type:'POST',
	        url:'http://www.roh.nurmaan.free-staging-server.us/api/going',
	        dataType:'json',
	        data:{user_id:user_id, schedule_id:schedule_id},
	        success:function(data){
                if(data.result){
                	console.log(data.data);
                	download('view/planner.html', func['planner']);

                }else{
                	notif('Failed to load data. Please try again');
                }

	        },
	        error:function(data){
	            notif('Failed to load data. Please try again');
	        }
	    });
	}, function(){
		notif('Sorry, could not process your request because you are offline');
	});	
}
function pln_not_going(schedule_id){
	 online(function(){
		$.ajax({
	        type:'POST',
	        url:'http://www.roh.nurmaan.free-staging-server.us/api/not_going',
	        dataType:'json',
	        data:{user_id:user_id, schedule_id:schedule_id},
	        success:function(data){
                if(data.result){
                	console.log(data.data);
                	download('view/planner.html', func['planner']);

                }else{
                	notif('Failed to load data. Please try again');
                }

	        },
	        error:function(data){
	            notif('Failed to load data. Please try again');
	        }
	    });
	}, function(){
		notif('Sorry, could not process your request because you are offline');
	});	
}

function pln_submit(case_id){
	var elem = document.getElementsByClassName('pln-sub');
	console.log(elem);
	var data = {};
	var conti = true;
	for(var i =0; i<elem.length; i++){

		if(elem[i].id.indexOf('pln-aud') >= 0){
            if(document.getElementById('pln-aud-pri').checked){
            	state = 2
            }else if(document.getElementById('pln-aud-pri').checked){
            	state = 1;
            }else{
            	conti = false;
            }

	    }else{
	    	if(elem[i].value == '' || typeof elem[i].value == 'undefined'){
            	console.log(elem[i].name);
	    		conti = false;
	    		if(elem[i].name == 'case_id'){
	    			conti = true;
	    		}
	    	}else{
	    		data[elem[i].name] = elem[i].value;
	    	}
	    }	
	}
	if(typeof data['case_id'] != 'undefined' && data['case_id'].indexOf('sg') >=0){
		data['stage'] = true;
		data['case_id'] = data['case_id'].replace('sg','');
	}
	console.log(data);

	if(conti){
		console.log(data);
		online(function(){
			$.ajax({
		        type:'POST',
		        url:'http://www.roh.nurmaan.free-staging-server.us/api/add_schedule',
		        dataType:'json',
		        data:{user_id:user_id, data:JSON.stringify(data)},
		        success:function(data){
	                if(data.result){
	                	hide_pln_add();
	                	notif('Event created');
	                }else{
	                	notif('Failed to load data. Please try again');
	                }

		        },
		        error:function(data){
		            notif('Failed to load data. Please try again');
		        }
		    });
		}, function(){
			notif('Sorry, could not complete the process because you are offline');
		});	
	}else{
		notif('All fields have to be filled');
	}

}

function pln_cancel_event(schedule_id){
	online(function(){
		$.ajax({
	        type:'POST',
	        url:'http://www.roh.nurmaan.free-staging-server.us/api/cancel_schedule',
	        dataType:'json',
	        data:{user_id:user_id, schedule_id:schedule_id},
	        success:function(data){
                if(data.result){
                	notif('Event cancelled');

                	download('view/planner.html', func['planner']('my'));

                }else{
                	notif('Failed to load data. Please try again');
                }

	        },
	        error:function(data){
	            notif('Failed to load data. Please try again');
	        }
	    });
	}, function(){
		notif('Sorry, could not process your request because you are offline');
	});	
}

function init_pln_listener(){
	$('.pln-ext-btn').click(function(e){
		var id = this.id.substr(this.id.indexOf('-') + 1);
		var state = this.innerHTML;
		console.log(id);


		if(state == '+'){			
		    $('#pln-cont-' + id).animate({'opacity':'0'}, 400).animate({'opacity':'1'},400);
		    setTimeout(function(){
		    	$('#pln-cont-' + id).css({'height':'unset'});
		    	$('#pln-body-' + id).css({'display':'unset'});		    	
		    }, 500);
		    
		    document.getElementById('plnextbtn-' + id).innerHTML = '-';
		}else if(state == '-'){
            $('#pln-cont-' + id).animate({'height':'1.2em'}, 300);
            setTimeout(function(){$('#pln-body-' + id).css({'display':'none'});},200);
		    document.getElementById('plnextbtn-' + id).innerHTML = '+';
		}

	});

	$('#pln-cir').click(function(e){
        document.getElementById('pln-add').style.display = 'unset';
	});

	$('#pln-add-x').click(function(e){
		hide_pln_add();
	});

	$('#pln-getloc').click(function(e){
		console.log(this.id);
		getLocation(function(position){
			var geo = position.coords.latitude + ',' + position.coords.longitude;
			document.getElementById('lbl-pln-getloc').value = geo;
		}, locationError, true);
	});

	$('.pln-openmap').click(function(e){
		var id = this.id.substr(this.id.indexOf('-') + 1);
		console.log(id);
        console.log(document.getElementById('coord-'+id).innerHTML);
        console.log( document.getElementById('pln-title-'+id).innerHTML);
		show_map("","", document.getElementById('coord-'+id).innerHTML, document.getElementById('pln-title-'+id).innerHTML)
	});
}

function show_pln_add(options){
    $('#pln-add').css({'display':'unset'});
    $('#pln-add').animate({'opacity':'1'}, 500);
    setTimeout(function(){
    	$('#pln-add-cont').animate({'top':'-10px'}, 500).animate({'top':'8px'}, 200).animate({'top':'0px'}, 200);
    }, 250);

    console.log(options);
    if(typeof options != 'undefined'){
		for(var i=0; i<Object.keys(options).length;i++){
			console.log(options[i]);
			if(options[i]['elem'].indexOf('pln-aud') >= 0){
				
				document.getElementById(options[i]['elem']).checked = true;
			}else{
				try{
	               document.getElementById(options[i]['elem']).value = options[i]['value'];
				}catch(e){

				}
				
			}
			
		}	
    }


    init_pln_listener();

}

function hide_pln_add(){
    
    $('#pln-add-cont').animate({'top':'-10px'}, 200).animate({'top':'100vh'}, 500);
    setTimeout(function(){
    	$('#pln-add').animate({'opacity':'0'}, 300);
    }, 250);     
    setTimeout(function(){
    	$('#pln-add').css({'display':'none'});
    }, 700); 
}