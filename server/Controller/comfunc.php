<?php
function controller($name, $blink)
{
    try
	{
		include_once("Controller/$name");
	}
    catch(Exception $e)
	{
		//file_get_contents($link);
	}
}

function view($name)
{
	
	try 
	{
		echo(file_get_contents("Views/$name"));
	}
	catch(Exception $e)
	{
		print("Could not load the view $name");
	}
}

function model($name)
{
	
    try 
	{
		include_once("Model/$name");
	}
	catch(Exception $e)
	{
		print("Could not load the model $name");
	}
}